<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>职位管理</title>
<%@ include file="/common/includePub.jsp"%>
<script type="text/javascript">
	$(function() {
		$("#postion_table").datagrid({
			//url : "${ctx}/sys/listUserPos.do?uid=${uid}",
			url:"${ctx}/sys/listPos.do",
			idField : "posId",
			title : "职位信息",
			rownumbers : true,
			pagination : true,
			columns : [ [ {
				field : "posId",
				title : "职位编码",
				checkbox : true
			}, {
				field : "posId2",
				title : "职位编码",
				width : 100
			}, {
				field : "posName",
				title : "职位名称",
				width : 200
			} ] ],
			height : 350,
			toolbar : [ {
				text : "保存",
				handler : function() {					 
					//判断是否选中职位
					var cks = $("#postion_table").datagrid("getChecked");
					if(cks==null || cks.length==0){
						$.messager.alert("提示","请选择一行记录操作！");
						return false;
					}
					var arr = new Array();
					for(var i=0;i<cks.length;i++){
						arr.push(cks[i].posId);	
					}				
					var url = "${ctx}/sys/grantUserByPos.do";
					jQuery.ajax({url : url, //连接
						type : "POST",
						data : { //传输数据
							uid :"${uid}",
							pids :arr
						},
						success : function(data) { //成功执行
							 $.messager.alert(data);
						},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							$.messager.alert("提示", "错误：" + errorThrown);
						},
						dataType : "text"
					});
					 
					
					
					
					
				}
			} ],
			onLoadSuccess : function(data) {
				//职位加载成功后，ajax请求用户已经拥有的职位信息
				jQuery.post("${ctx}/sys/listUserPos.do?uid=${uid}",function(data){
					if(data!=null && data!=undefined){
						var rows = data;
						for(var i=0;i<rows.length;i++){
							//调用datagrid，selectRecord
							$("#postion_table").datagrid("selectRecord",rows[i]);
						}
					}
				},"json");
			}
		});

	});
</script>
</head>
<body>

	<table id="postion_table"></table>
	<div id="win" style="display: none;">
		<iframe id="winFrame" align="top" frameborder="0" scrolling="auto"
			marginheight="0" marginwidth="0" title="内容区"></iframe>
	</div>
</body>
</html>