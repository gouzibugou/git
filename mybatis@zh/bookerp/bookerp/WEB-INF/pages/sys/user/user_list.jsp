<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
<%@ include file="/common/includePub.jsp" %>
<script type="text/javascript">
$(function(){
	$("#postion_table").datagrid({
	    url:"${ctx}/sys/listUser.do",
		title:"用户管理",rownumbers:true,pagination:true,
		columns:[[{field:"uid",title:"用户标识",checkbox:true},
			{field:"uid2",title:"编号",width:150},
			{field:"xm",title:"姓名",width:200},
			{field:"cjsj",title:"创建时间",width:150},
		{field:"zt",title:"状态",width:100,formatter:function(val,row,idex){
			if(val==0){
				return "可用";
			}
			return  "不可用";
		}},{field:"bz",title:"备注",width:200}
		 ]],height:350,
		 toolbar:[{
			 text:"新增",handler:function(){
				 
				 /*
				 $("#win").dialog({
					 title:"新建职位",modal:true,
					 width:400,
					 height:300,
					 closed:true
				 });
				 $("#winFrame").css({
					 width:"400px",
					 height:"300px"
				 });
				 $("#winFrame").attr("src","${ctx}/sys/addPos.do");
				 $("#win").dialog("open");
				 */
				// $.messager.alert("aa","bbbbb");
			 }
		 },"-",{
			 text:"编辑"
		 },"-",{
			 text:"查看"
		 },"-",{
			 text:"删除"
		 },"-",{
			 text:"授予职位",handler:function(){
				 
				 var cks = $("#postion_table").datagrid("getChecked");
				 if(cks==null || cks.length==0){
					 $.messager.alert("提示","请选择一行记录操作！");
					 return false;
				 }
				 if(cks.length>1){
					 $.messager.alert("提示","该操作暂时不支持批量！");
					 return false;
				 }
				  
				 $("#win").dialog({
					 title:"用户授予职位",modal:true,
					 width:400,
					 height:300,
					 closed:true
				 });
				 $("#winFrame").css({
					 width:"400px",
					 height:"300px"
				 });
				 
				 $("#winFrame").attr("src","${ctx}/sys/grantUserPos.do?uid="+cks[0].uid);
				 $("#win").dialog("open");
				 
				// $.messager.alert("aa","bbbbb");
			 }
		 }]
	});
	
	/*
	$("#win").dialog({
		title:"新建",
		width:400,
		height:300,closed:true,		
	});*/
	
});

function closeWin(){
	//关闭弹出框并且刷新datagrid
	$("#win").dialog("close");
	$("#postion_table").datagrid("reload");
}
</script>
</head>
<body>
<table id="postion_table"></table>
<div id="win" style="display:none;">
<iframe  id="winFrame" align="top" frameborder="0" scrolling="auto" marginheight="0" marginwidth="0" title="内容区"></iframe>
</div>
</body>
</html>