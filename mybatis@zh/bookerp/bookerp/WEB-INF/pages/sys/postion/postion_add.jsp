<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新增职位</title>
<%@ include file="/common/includePub.jsp"%>
<script type="text/javascript">
	$(function() {

		$("#btmSave").click(function() {
			$("#posIdSpan").removeClass("no");
			/**
			 *非空验证
			 *1、先验证职位编码是否存在，如果存在，在文本框后面提示已存在
			 *2、验证通过后提交
			 */
			var  posId = $("#posId").textbox("getValue");
			var posName = $("#posName").textbox("getValue");
			if(posId==null || jQuery.trim(posId)==""){				 
				$.messager.alert("提示","请输入职位编码！");
				return false;
			}
			if(posName==null || jQuery.trim(posName)==""){				
				$.messager.alert("提示","请输入职位编码！");
				return false;
			}
			//声明方法
			var saveFqn = function(){
				var url = "${ctx }/sys/savePos.do";
				jQuery.ajax({url : url, //连接
					type : "POST",
					data : { //传输数据
						posId : $("#posId").textbox("getValue"),
						posName : $("#posName").textbox("getValue")
					},
					success : function(data) { //成功执行
						if (data != null && data == "success") {
							$.messager.alert("提示", "新增成功！", "info", function() {
								//点击确认后关闭页面（调用父窗口方法postion_list页面方法closeWin）
								window.parent.closeWin();
							});
						} else {
							$.messager.alert("提示", "新增失败！");
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						$.messager.alert("提示", "错误：" + errorThrown);
					},
					dataType : "text"
				});
			}
			
			
			jQuery.ajax({url :"${ctx }/sys/checkPos.do", //连接
				type : "POST",
				data : { //传输数据
					posId :posId					 
				},
				success : function(data) { //成功执行
					if (data != null && data == "0") {
						//不存在就保存
						saveFqn();
						$("#posIdSpan").addClass("chk-ok");
						$.messager.alert("提示", data);
					} else {
						$("#posIdSpan").addClass("chk-no");
						$.messager.alert("提示", "职位编码存在！");
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					$.messager.alert("提示", "错误：" + errorThrown);
				},
				dataType : "text"
			});
		});
	});
</script>
</head>
<body>
	<form id="frm" action="#" method="post">
		职位编码：<input type="text" class="easyui-textbox" id="posId" /><span
			id="posIdSpan">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br /> 职位名称：<input type="text"
			class="easyui-textbox" id="posName" /><br /> <a href="#"
			class="easyui-linkbutton" id="btmSave">保存</a>
	</form>
</body>
</html>