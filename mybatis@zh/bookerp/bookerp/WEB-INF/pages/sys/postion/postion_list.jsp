<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>职位管理</title>
<%@ include file="/common/includePub.jsp" %>
<script type="text/javascript">
$(function(){
	$("#postion_table").datagrid({
	    url:"${ctx}/sys/listPos.do",
		title:"职位信息",rownumbers:true,pagination:true,
		columns:[[{field:"posId",title:"职位编码",checkbox:true},
			{field:"posId2",title:"职位编码",width:200},
			{field:"posName",title:"职位名称",width:200},
		{field:"status",title:"状态",width:200,formatter:function(val,row,idex){
			if(val==0){
				return "可用";
			}
			return  "不可用";
		}}
		 ]],height:350,
		 toolbar:[{
			 text:"新增",handler:function(){
				 
				 
				 $("#win").dialog({
					 title:"新建职位",modal:true,
					 width:400,
					 height:300,
					 closed:true
				 });
				 $("#winFrame").css({
					 width:"400px",
					 height:"300px"
				 });
				 $("#winFrame").attr("src","${ctx}/sys/addPos.do");
				 $("#win").dialog("open");
				 
				// $.messager.alert("aa","bbbbb");
			 }
		 },"-",{
			 text:"编辑"
		 },"-",{
			 text:"查看"
		 },"-",{
			 text:"删除"
		 },"-",{
			 text:"授予菜单",handler:function(){
				 var cks = $("#postion_table").datagrid("getChecked");
				 if(cks==null || cks.length==0){
					 $.messager.alert("提示","请选择一行记录操作！");
					 return false;
				 }
				 if(cks.length>1){
					 $.messager.alert("提示","该操作暂时不支持批量！");
					 return false;
				 }
				  
				 $("#win").dialog({
					 title:"职位授予菜单",modal:true,
					 width:400,
					 height:600,
					 closed:true
				 });
				 $("#winFrame").css({
					 width:"400px",
					 height:"600px"
				 });
				 
				 $("#winFrame").attr("src","${ctx}/sys/grantMenu.do?posId="+cks[0].posId);
				 $("#win").dialog("open");
			 }
		 },'-',{
			 text:"职位导出",handler:function(){
				 $("#expData").attr("action","${ctx}/sys/expPos.do?_t="+new Date().getTime());
				 $("#expData").attr("target","_blank");
				 $("#expData").submit();
			}
		 }]
	});
	
	/*
	$("#win").dialog({
		title:"新建",
		width:400,
		height:300,closed:true,		
	});*/
	
});

function closeWin(){
	//关闭弹出框并且刷新datagrid
	$("#win").dialog("close");
	$("#postion_table").datagrid("reload");
}
</script>
</head>
<body>
<form id="expData"></form>
<div id="search-panel" class="easyui-panel" title="查询" iconCls="icon-search">
<table>
	<tr>
		<Td>职位名称</Td>
		<Td><input type="text" class="easyui-textbox" /></Td>
	</tr>
	<tr>
		<Td><a href="#" class="easyui-linkbutton">查询</a></Td>
		<Td> </Td>
	</tr>
</table>

</div>
<div>&nbsp;</div>
<div>
<table id="postion_table"></table></div>
<div id="win" style="display:none;">
<iframe  id="winFrame" align="top" frameborder="0" scrolling="auto" marginheight="0" marginwidth="0" title="内容区"></iframe>
</div>
</body>
</html>