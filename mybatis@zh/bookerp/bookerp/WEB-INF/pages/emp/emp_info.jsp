<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工基本信息</title>
</head>
<body>
<style type="text/css">
.txt01{width:100px;line-height:18px;}
</style>
<input type="hidden" id="empId" name="empId" value="${empId }"/>
	<table>
		<tr>
			<td>姓名</td>
			<td><input type="text" id="xm"  class="txt01" /></td>
			<td>汉语拼音</td>
			<td><input type="text" id="hypy" class="txt01" /></td>
			<td>性别</td>
			<td><input type="radio" name="sex" value="1" />男&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="sex" value="0" />女</td>
		</tr>
		<tr>
			<td>出生日期</td>
			<td><input type="text" id="csrq" class="txt01" /></td>
			<td>籍贯</td>
			<td><input type="text" id="jg" class="txt01" /></td>
			<td>政治面貌</td>
			<td><select class="txt01" id="zzmm"><option>--请选择--</option></select></td>
		</tr>
		<tr>
			<td colspan="3">
				<a href="#" class="easyui-linkbutton" 
				data-options="iconCls:'icon-save'" onclick="saveInfo()">保存</a>
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="easyui-linkbutton"
				 onclick="closeWin()" data-options="iconCls:'icon-canel'">取消</a>
			</td>
		</tr>
	</table>
<script type="text/javascript">
/**
 * 保存数据
 */
function saveInfo(){
	//验证数据完整性
	
	
	jQuery.ajax({
		method:"post",
		url:"${pageContext.request.contextPath}/emp/saveEmp.do?page=info",
		data:{
			empId:"${empId}",
			xm:$("#xm").val(),
			hypy:$("#hypy").val(),
			sex:$("input[name=sex]:checked").val(),
			zzmm:$("#zzmm").val()
		},
		success:function(data){
			alert(data);
		}
	});
	
	
}
</script>
</body>
</html>