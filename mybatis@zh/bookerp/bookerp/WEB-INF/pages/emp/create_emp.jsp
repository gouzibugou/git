<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新员工登记</title>
<%@ include file="/common/includePub.jsp"%>
<style type="text/css">
div{width:800px;}
</style>
<script type="text/javascript">
	$(function() {
		var flag="${flag}";
		if(flag=="new"){
			//$("#sub_empInfo_null").show();
			//$("#sub_empInfo").hide();
			//$(".other-info").css("display","none");
		}
		
		$("#empInfo").panel({
			tools: [{    
			    iconCls:'icon-edit',    
			    handler:function(){
			    	var url = "${ctx}/emp/editemp.do?page=info&empId=${empId}"
			    	var win = window.parent.openWin("基本信息编辑",url,500,300,function(){
			    		$("#sub_empInfo_null").hide();
						$("#sub_empInfo").show();
						$(".other-info").css("display","block");
			    		//1、ajax远程加载赋值
			    		//$("#frm_empInfo").form("load","${ctx}/emp/queryEmp.do?
			    		//page=info&empId=${empId}");
						
			    		//2、赋值值
			    		
						loadInfo();
			    	
			    	
			    	});
			    	 
			    }    
			  }]
		});
		
		function loadInfo(){
			jQuery.post("${ctx}/emp/queryEmp.do?page=info&empId=${empId}",
    				function(data){
    			if(data!=null && data!=undefined){
    				$("#sp_hypy").text(data.xm);
    				
    				//$("#frm_empInfo").form("load",{
		    		//	xm:data.xm
		    		//});
    			}
    			
    		},"json");
			
		}
		
		$("#tb_emp_homes").datagrid({
			height:300,width:500,
			url:"${ctx}/emp/listEmpHome.do?empId=${empId}",
			data:[{id:"1",xm:"张三"}],
			columns:[[{field:"id",title:"-",hidden:true},
			          {field:"xm",title:"姓名",width:80},
			          {field:"gx",title:"关系",width:50},
			          {field:"gzdw",title:"工作单位",width:100},
			          {field:"lxfs",title:"联系方式",width:100}
			          ,{field:"bz",title:"备注",width:100}
			          ,{field:"op",title:"<a href=\"#\" onclick=\"test()\">+</a>",width:80,formatter:function(val,row,idx){
			        	  return "<a href=\"#\">删除</a>"
			          }}]]
		});
		
		  
	});
	
	function test(){
		var url = "${ctx}/emp/editemp.do?page=home&empId=${empId}";
		var win = window.parent.openWin("家庭成员编辑",url,500,300,function(){
			$("#tb_emp_homes").datagrid("reload");
		});
	};
</script>
</head>
<body class="easyui-panel">
	<div>
		<h3>入职登记表</h3>
	</div>
	<div id="empInfo" class="easyui-panel" data-options="title:'员工基本信息',
	collapsible:true">
		<div id="sub_empInfo_null"><a href="#">新建</a></div>
		<div id="sub_empInfo" ><form id="frm_empInfo"><table>
			<tr>
				<td>姓名</td>
				<td><input type="text" id="xm" name="xm"/></td>
				<td>汉语拼音</td>				
				<td><span id="sp_hypy"></span></td>
				<td>性别</td>
				<td>男</td>
				<td rowspan="2"><img  src="${ctx }/resource/image/kaola.jpg"
				 width="120" height="160"/></td>
			</tr>
			<tr>
				<td>出生日期</td>
				<td>1999-09-09</td>
				<td>籍贯</td>				
				<td>湖北武汉</td>
				<td>政治面貌</td>
				<td>群众</td>				 
			</tr>
		</table></form></div>
	</div>
	<div class="other-info">
	<div>&nbsp;</div>
	<div class="easyui-panel" data-options="title:'健康状况',collapsible:true">
	11111</div>
	<div>&nbsp;</div>
	<div class="easyui-panel" style="height:300px;" data-options="title:'家庭成员',collapsible:true">
		<table id="tb_emp_homes"></table>
	</div>
<div>&nbsp;</div>
	<div class="easyui-panel" data-options="title:'教育情况',collapsible:true"></div>
<div>&nbsp;</div>
	<div class="easyui-panel" data-options="title:'工作情况',collapsible:true"></div>
<div>&nbsp;</div>
	<div class="easyui-panel" data-options="title:'入职信息',collapsible:true"></div>
</div>
 
</body>
</html>