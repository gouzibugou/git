<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工列表</title>
<%@ include file="/common/includePub.jsp"%>
 
<script type="text/javascript">
	$(function() {
		 $("#emp_tabs").datagrid({
			 title:"员工信息列表",
			 url:"${ctx}/emp/listEmp.do",
			 columns:[[{field:"empId",title:"-",checkbox:true},
			           {field:"empNo",title:"员工编号",width:100},
			           {field:"empName",title:"姓名",width:100}
			           ]],
			 pagination:true,
			 toolbar:[{
				 text:"编辑",handler:function(){
					 var cks = $("#emp_tabs").datagrid("getChecked");
					 if(cks==null || cks.length==0){
						 $.messager.alert("提示","请选择一行！");
						 return false;
					 }
					 if(cks.length>1){
						 $.messager.alert("提示","该操作暂时不支持批量！");
						 return false;
					 }
					 //跳转编辑页面
					 $("#frm").attr("action","${ctx}/emp/createemp.do?empId="+cks[0].empId);
					 $("#frm").submit();
				 }
			 },{
				 text:"查看"
			 }]
		 });
		  
	});
</script>
</head>
<body>
<form id="frm" method="post"></form>
<div id="search-panel" class="easyui-panel" title="查询" iconCls="icon-search">

</div>
<div id="data-panel">
<table id="emp_tabs"></table>
</div>
</body>
</html>