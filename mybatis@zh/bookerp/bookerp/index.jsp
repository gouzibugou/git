<%@page import="bookerp.WebContrants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="cc" uri="/WEB-INF/tld/c.tld"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%    Object obj = session.getAttribute(WebContrants.USER_SESSION_KEY);
if(obj==null){
	  response.sendRedirect("toLogin.do");
} %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
<%@ include file="/common/includePub.jsp"%>
<style type="text/css">
.sub-menu{list-style:none;font-size:14px;}
.sub-menu li {line-height:25px;}
</style>
<script type="text/javascript">
	$(function() {
		$(".sub-menu a").click(function(){
			var title = $(this).text();	
			var url = $(this).attr("url");
			var tab = $("#mytabs");
			//如果选项卡不存在，就新建
			if(!tab.tabs("exists",title)){
				var html= "<iframe frameborder=\"0\" align=\"top\" ";
				html += "marginheight=\"0\" marginwidth=\"0\" ";
				html += " src=\"${ctx}/"+url+"\" ";
				html += " style=\"width:900px;height:400px;\" ";
				html += " title=\"内容区\" ></iframe>";
				
				tab.tabs("add",{
					id:"tb-"+new Date().getTime(),
					title:title,
					closable:true,
					content:html
				});
			}else{
				//选项卡存在就选中
				tab.tabs("select",title);
			}
			
		});
		
		$("#winForm").dialog({    
    	    title: "基本信息编辑",    
    	    width: 500,    
    	    height: 300,closed:true    	     
    	}); 
		
		
	});
	
	function openWin(title,url,width,height,obj){
    	return $("#winForm").dialog({    
    	    title: title,    
    	    width: width,    
    	    height: height,    
    	    closed: false,    
    	    cache: false,    
    	    href: url,    
    	    modal: true,
    	    onClose:obj
    	});  
	}
	
	function closeWin(){
		$("#winForm").dialog("close");
	}
	
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',title:'',split:false"
		style="height: 80px;">
		<h1>北京新华书店ERP管理系统</h1>
	</div>
	<div data-options="region:'south',title:'',split:false"
		style="height: 40px; text-align: center; padding-top: 10px;">版权所有&copy;2018&nbsp;&nbsp;北京新华书店集团</div>
	<div id="mytabs" data-options="region:'center',title:'',border:false"
		style="padding: 0px; margin: 0px;" class="easyui-tabs">
		<div title="欢迎" style="padding: 5px;" data-options="href:'desc.html'"></div>
	</div>
	<div data-options="region:'west',title:'菜单导航',split:false"
		style="width: 230px;" class="easyui-accordion">
		<%-- 
		<div title="控制中心" data-options="iconCls:'icon-save'">
			<ul class="sub-menu">
				<li><a href="#">用户管理</a></li>
				<li><a href="#">职位管理</a></li>			 
			</ul>
		</div>
		<div title="图书管理" data-options="iconCls:'icon-reload',selected:true">
			<ul class="sub-menu">
				<li><a href="#">图书类别管理</a></li>
				<li><a href="#">图书信息管理</a></li>			 
			</ul>
		</div>--%>
		<div style="display:none;"><ul>
		<c:forEach items="${menuList }" var="m">
			<c:if test="${m.isLeaf==0 }">			
			</ul></div>
			<div title="${m.mc }" data-options="iconCls:'icon-reload'">
				<ul class="sub-menu">			
			</c:if>
			<c:if test="${m.isLeaf==1 }">
				<li><a href="#" url="${m.url }">${m.mc }</a></li>
			</c:if>		
		</c:forEach>
		</ul></div>


	</div>
	<div id="winForm"></div>
	<ul style="display: none;">
		<li><a href="${ctx }/sys/mgrPos.do">职位管理</a></li>
		<li><a href="${ctx }/sys/mgrUser.do">用户管理</a></li>
	</ul>
</body>
</html>