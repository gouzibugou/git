<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><% request.setAttribute("ctx",request.getContextPath());%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录页面</title>
<link rel="stylesheet" type="text/css" href="${ctx }/resource/image/style.css"></link>
<link rel="stylesheet" type="text/css" href="${ctx }/resource/js/jquery-eayui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="${ctx }/resource/js/jquery-eayui/themes/icon.css" />
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/resource/js/jquery-eayui/jquery.easyui.min.js"></script>
<style type="text/css">
html,body{padding:0px;margin:0px;text-align:center;}
.td01,.td02{line-height:30px;}
.td01{width:150px;text-align:right;padding-right:5px;}
.td02{width:250px;text-align:left;padding-left:5px;}
</style>
<script type="text/javascript">
	$(function() {

	});
	//登录
	function toLogin(){
		var loginName = $("#loginName").val();
		var pwd = $("#loginPwd").val();
		//完成非空验证
		
		jQuery.ajax({
			url:"${ctx}/login.do",
			data:{loginName:loginName,loginPwd:pwd},
			success:function(data){
				if(data!=null && data=="success"){
					$("#frm").attr("action","${ctx}/main.do");
					$("#frm").submit();
				}else{
					$.messager.alert("登录提示",data);
				}
			}
		});
	}
</script>
</head>
<body style="text-align:center;margin:auto;">
<form id="frm" method="post"></form>
	<div id="login" class="easyui-panel"
	 style="text-align:center;width:400px;height:200px;
	 margin-left:auto;margin-right:auto;" title="系统登录">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="td01">用户名：</td>
				<td class="td02"><input id="loginName" type="text" 
				class="easyui-validatebox" value="admin" /></td>
			</tr>
			<tr>
				<td class="td01">密码：</td>
				<td class="td02"><input id="loginPwd" type="password"
				 class="easyui-validatebox"  value="123456" /></td>
			</tr>
			<tr>				 
				<td colspan="2"><a href="#" class="easyui-linkbutton" 
				onclick="toLogin();">&nbsp;&nbsp;&nbsp;登录&nbsp;&nbsp;&nbsp;</a></td>
			</tr>
		</table>
	</div>
</body>
</html>