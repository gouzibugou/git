package bookerp;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import bookerp.service.sys.UserService;

public class Test {
	public static void main(String[] args) {
		try {
			ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
			UserService us = app.getBean(UserService.class);
			List<Map<String,Object>> list = us.findUserPosAndpos("2");
			System.out.println(JSONArray.fromObject(list).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
