package bookerp.controller.emp;

import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.lf5.PassingLogRecordFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookerp.controller.BaseController;
import bookerp.vo.EmpVo;

@Controller
@RequestMapping("/emp")
public class EmplooyController extends BaseController {

	private static Log logger = LogFactory.getLog(EmplooyController.class);
	
	@RequestMapping("/createemp.do")
	public ModelAndView toAdd(String empId){
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName(PATH_SP+"emp/create_emp");
		mnv.addObject("empId",empId);	
		mnv.addObject("flag","old");
		if(empId==null || "".equals(empId.trim())){
			//当empId为null，新增员工，不为空，编辑员工信息
			String uid = UUID.randomUUID().toString();
			mnv.addObject("empId",uid);	
			mnv.addObject("flag","new");
		}
		return mnv;
	}
	
	/**
	 * 编辑信息
	 * @return
	 */
	@RequestMapping("/editemp.do")
	public ModelAndView editemp(String page,String empId){
		logger.debug(page);
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName(PATH_SP+"emp/emp_"+page);		 
		mnv.addObject("empId",empId);
		//能根据id查询不同面版信息
		
		
		return mnv;
	}
	
	@RequestMapping("/saveEmp.do")
	@ResponseBody
	public String saveemp(EmpVo emp){
		try {
			if(emp!=null){
				logger.debug(emp.getEmpId());
			}
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping("/queryEmp.do")
	@ResponseBody
	public String queryInfo(String empId,String page){
		String retObj = "{}";
		try {			
			if(page==null || "".equals(page.trim())){
				throw new NullPointerException("缺少查询参数");
			}
			if(empId==null || "".equals(empId.trim())){
				throw new NullPointerException("缺少查询参数");
			}
			if("info".equals(page)){
				//基本信息
			}else if("jkinfo".equals(page)){
				//健康信息
			}			
			//数据仅供参考
			retObj = "{\"empId\":\""+empId+"\",\"xm\":\"测试\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug(retObj);
		return retObj;
	}
	
	
	/**
	 * 跳转员工管理页面
	 * @return
	 */
	@RequestMapping("/mgrEmp.do")
	public String mgrEmp(){
		
		return PATH_SP+"emp/emp_list";
	}
	
	/**
	 * 员工管理页面列表查询
	 * @return
	 */
	@RequestMapping("/listEmp.do")
	@ResponseBody
	public String listEmp(){
		try {
			String str = "{\"total\":\"2\",\"rows\":[";
			str += "{\"empId\":\"11111111111\",\"empNo\":\"R0001\",\"empName\":\"张三\"},";
			str += "{\"empId\":\"22222222222\",\"empNo\":\"R0002\",\"empName\":\"李四\"}";
			str += "]}";
			logger.debug(str);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"total\":0,\"rows\":[]}";
	}
	
	
	
}
