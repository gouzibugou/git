package bookerp.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
 
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import bookerp.WebContrants;
import bookerp.dao.sys.UserDao;
import bookerp.entity.SysUser;
import bookerp.service.sys.UserService;
import bookerp.vo.UserVo;


@Controller
public class LoginController extends BaseController{
	
	private static final Log logger = LogFactory.getLog(LoginController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	@RequestMapping("/toLogin.do")
	public String toLogin(){
		logger.debug("进去登录页面............");
		return "/login";
	}
	
	@RequestMapping("/login.do")
	@ResponseBody
	public String login(HttpServletRequest request,String loginName,String loginPwd){
		try {
			SysUser u = userService.findUserByLNamePwd(loginName,loginPwd);
			if(u==null){
				return "用户名或密码错误，登录失败！";
			}
			//把用户放如会话信息中
			UserVo uv = new UserVo();
			uv.setUid(u.getUid());
			uv.setLoginName(u.getLoginName());
			uv.setXm(u.getXm());
			//查询用户职位
			List<String[]> list = userService.findUserPosByUid(u.getUid());
			if(list!=null){
				Integer p[] = new Integer[list.size()];
				String[] pn = new String[list.size()];
				for (int i = 0; i < list.size(); i++) {
					
					 p[i] = Integer.parseInt(list.get(i)[0]);
					 pn[i] = list.get(i)[1];
					 
				}
				uv.setPosIds(p);
				uv.setPosNames(pn);
			}
			request.getSession().setAttribute(WebContrants.USER_SESSION_KEY,uv);			
			return "success";			
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
	}
	
	
}
