package bookerp.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.WebUtils;

import bookerp.WebContrants;
import bookerp.entity.SysUser;
import bookerp.vo.UserVo;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

/**
 * ����controller
 * ��������
 * @author Administrator
 *
 */
public abstract class BaseController {

	protected static final String PATH_SP = "/WEB-INF/pages/";
	
	
	public String toDateJson(Object obj){
		try {
			JsonConfig jc = new JsonConfig();
			jc.registerJsonValueProcessor(Date.class,new JsonValueProcessor() {
				
				@Override
				public Object processObjectValue(String p, Object val, JsonConfig jc) {
					if(val!=null){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						if(val instanceof Date){
							return sdf.format((Date)val);
						}
						if(val instanceof java.sql.Date){
							return "2018-10-19 10:10:10";
						}
						
					}					
					return null;
				}
				
				@Override
				public Object processArrayValue(Object arg0, JsonConfig arg1) {
					 
					return null;
				}
			});
			return JSONObject.fromObject(obj,jc).toString();
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * 获取当前登录用户信息
	 * @param request
	 * @return
	 */
	protected UserVo getUser(HttpServletRequest request){
		Object obj = WebUtils.getSessionAttribute(request,WebContrants.USER_SESSION_KEY);
		if(obj!=null){
			return (UserVo) obj;
		}
		return null;
	}
	
	
	
	
}
