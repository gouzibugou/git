package bookerp.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import bookerp.entity.SysMenu;
import bookerp.entity.SysUser;
import bookerp.service.sys.UserService;
import bookerp.vo.UserVo;

@Controller
public class SysMainController extends BaseController {

	@Resource(name="userService")
	private UserService userService;
	
	
	@RequestMapping("/main.do")
	public ModelAndView main(HttpServletRequest req){
		ModelAndView mnv = new ModelAndView("/index");
		try {
			//先判断用户是否登录
			UserVo u = this.getUser(req);
			if(u==null){
				mnv.setViewName("/login");
			}
			//查询用户对应职位及菜单
			List<SysMenu> list = userService.findMenuByUid(u);
			mnv.addObject("menuList",list);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mnv;
	}
	
	
}
