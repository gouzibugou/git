package bookerp.controller.sys;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
 

import org.springframework.web.servlet.ModelAndView;

import bookerp.controller.BaseController;
import bookerp.entity.Postion;
import bookerp.service.sys.PostionService;
import bookerp.util.ExportExcels;
 

@Controller
@RequestMapping("/sys")
public class PostionController extends BaseController {

	private static Log logger = LogFactory.getLog(PostionController.class);

	@Resource(name = "postionService")
	private PostionService postionService;

	/**
	 * ְλ��ҳ
	 * 
	 * @return
	 */
	@RequestMapping("/mgrPos.do")
	public String index() {
		logger.debug("return postion list page!");
		return PATH_SP + "sys/postion/postion_list";
	}

	/**
	 * ��תְλ���
	 * 
	 * @return
	 */
	@RequestMapping("/addPos.do")
	public String add() {
		logger.debug("return postion add page!");
		return PATH_SP + "sys/postion/postion_add";
	}
	
	/**
	 * ����ְλ��Ϣ
	 * 
	 * @return
	 */
	@RequestMapping("/savePos.do")
	@ResponseBody
	public String save(HttpServletRequest request,Integer posId,String posName) {
		logger.debug("return postion save page!");
		try {
			logger.debug("===================");
			logger.debug(posId);
			logger.debug(posName);
			postionService.savePos(posId,posName);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		
		
		return "success";
	}
	
	/**
	 * ���ְλ�����Ƿ����
	 * 
	 * @return
	 */
	@RequestMapping("/checkPos.do")
	@ResponseBody
	public String checkPos(HttpServletRequest request,Integer posId) {
		logger.debug("return postion save page!");
		try {
			logger.debug("===================");
			logger.debug(posId);			 
			int rs = postionService.checkPostionUnique(posId);
			return rs + "";			
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}		 
	}
	
	
	
	/**
	 * ְλ�����б�
	 * 
	 * @return
	 */
	@RequestMapping("/listPos.do")
	@ResponseBody
	public String list() {
		String retStr = "{\"total\":\"0\",\"rows\":[]}";
		try {
			logger.debug("return postion list data!");
			List<Postion> list = postionService.findAll();
			if (list == null) {
				throw new NullPointerException("δ�ҵ��κ�ְλ����");
			}
			int c = list.size();
			// ����json�ַ���
			StringBuffer sb = new StringBuffer();
			sb.append("{\"total\":");
			sb.append(c);
			sb.append(",\"rows\":[");
			Postion pos = null;
			for (int i = 0; i < c; i++) {
				pos = list.get(i);
				sb.append(i > 0 ? "," : "");
				sb.append("{\"posId\":\"" + pos.getPosId() + "\",");
				sb.append("\"posId2\":\"" + pos.getPosId() + "\",");
				sb.append("\"posName\":\"" + pos.getPosName() + "\",");
				sb.append("\"status\":\"" + pos.getStatus() + "\"");
				sb.append("}");
			}
			sb.append("]}");
			retStr = sb.toString();
			logger.debug(retStr);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return retStr;
	}

	
	/**
	 * 跳转到给职位赋予菜单页面
	 * @param posId
	 * @return
	 */
	@RequestMapping("/grantMenu.do")
	public ModelAndView grantMenu(Integer posId){
		
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("posId",posId);
		
		//加载所有的菜单列表
		List<String[]> list = postionService.queryAllMenu();
		mnv.addObject("list",list);
		
		
		mnv.setViewName(PATH_SP+"sys/postion/postion_grant_menu");
		return mnv;
	}
	
	
	@RequestMapping("/getMenu.do")
	@ResponseBody
	public String getMenuByPid(String id){
		String retObj = "[]";
		try {
			logger.debug("pid:"+id);
			if(id==null || "".equals(id)){
				id = "root";
			}
			List<Map<String,Object>> list = 
					postionService.queryMenuByPid(id);
			retObj = JSONArray.fromObject(list).toString();
			logger.debug(retObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retObj;
	}
	
	
	@RequestMapping("/expPos.do")
	public void expData(HttpServletRequest request,HttpServletResponse response){
		try {
			logger.debug(response); 
			//取数据
			List<String[]> list = postionService.findAll(1);
			//生成workbook
			logger.debug(list);
			String[] title = new String[]{"编号","职位名称","状态"};
			//设置相应头
			String fn = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".xlsx";
			response.setContentType("application/vnd."
					+ "openxmlformats-officedocument.spreadsheetml.sheet");
			response.addHeader("Content-Disposition", "attachment;filename="+fn);
			response.setCharacterEncoding("UTF-8");
			ExportExcels.exportData(response.getOutputStream(),list,title);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
