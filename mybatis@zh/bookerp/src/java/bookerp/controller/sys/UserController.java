package bookerp.controller.sys;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.DefaultValueProcessor;
import net.sf.json.processors.DefaultValueProcessorMatcher;
import net.sf.json.processors.JsonValueProcessor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookerp.controller.BaseController;
import bookerp.entity.Postion;
import bookerp.service.sys.PostionService;
import bookerp.service.sys.UserService;
import bookerp.vo.PageBean;

/*
 * 用户管理
 */
@Controller
@RequestMapping("/sys")
public class UserController extends BaseController {

	private static Log logger = LogFactory.getLog(UserController.class);

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "postionService")
	private PostionService postionService;

	@RequestMapping("/mgrUser.do")
	public String mgrUser() {

		return PATH_SP + "/sys/user/user_list";
	}

	/**
	 * 跳转用户授予职位页面
	 * 
	 * @return
	 */
	@RequestMapping("/grantUserPos.do")
	public ModelAndView grantUserPos(String uid) {
		logger.debug("授权职位用户ID:" + uid);
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("uid", uid);
		mnv.setViewName(PATH_SP + "/sys/user/user_postion_list");
		return mnv;
	}

	/**
	 * 保存授权职位信息
	 * 
	 * @return
	 */
	@RequestMapping("/grantUserByPos.do")
	@ResponseBody
	public String grantUserByPos(HttpServletRequest request,String uid) {
		try {
			String pids[] = request.getParameterValues("pids[]");
			logger.debug("用户id:" + uid);
			logger.debug("职位：" + pids);
			 
			userService.saveUserGrantPosInfo(uid,pids);
			
			return "操作成功！";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "操作失败！";
	}

	/**
	 * 获取用户职位授权列表
	 * 
	 * @return
	 */
	@RequestMapping("/listUserPos.do")
	@ResponseBody
	public String listUserPos(HttpServletRequest request,String uid) {
		String retStr = "[]";
		try {
			//第一种
//			String retStr = "{\"total\":\"0\",\"rows\":[]}";
//			if (uid == null || "".equals(uid)) {
//				uid = "0";
//			}
//			List<Map<String, Object>> list = userService.findUserPosAndpos(uid);
//			if (list != null) {
//				Map<String, Object> json = new HashMap<String, Object>();
//				json.put("rows", list);
//				json.put("total", list.size());
//				retStr = JSONObject.fromObject(json).toString();
//				list = null;
//				json = null;
//			}
			
			//先判断是否为空，给默认值
			if (uid == null || "".equals(uid)) {
				uid = "0";
			}
			List<String> list = userService.findUserPosAndPos2(uid);
			if(list!=null){
				retStr = JSONArray.fromObject(list).toString();
			}
			logger.debug(retStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retStr;
	}

	/**
	 * 用户列表分页
	 * 
	 * @param request
	 * @param pageNo
	 * @param rows
	 * @return
	 */
	@RequestMapping("/listUser.do")
	@ResponseBody
	public String listUser(HttpServletRequest request, Integer pageNo,
			Integer rows) {
		String retStr = "{\"total\":\"0\",\"rows\":[]}";
		try {
			if (pageNo == null) {
				pageNo = 1;
			}
			if (rows == null) {
				rows = 10;
			}
			PageBean pb = userService.queryUserForPage(null, pageNo, rows);

			// 调用父类转换json方法
			retStr = this.toDateJson(pb);
			logger.debug(retStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retStr;
	}

}
