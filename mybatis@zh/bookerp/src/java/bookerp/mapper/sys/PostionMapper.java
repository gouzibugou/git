package bookerp.mapper.sys;

import java.util.List;
import java.util.Map;

import bookerp.entity.Postion;

public interface PostionMapper {

	public List<Postion> findAll();
	
}
