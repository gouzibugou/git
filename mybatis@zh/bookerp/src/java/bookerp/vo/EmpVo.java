package bookerp.vo;

import java.io.Serializable;

/**
 * 接收emp页面数据
 * @author Administrator
 *
 */
public class EmpVo implements Serializable{

	private String empId;
	private String xm;
	private String hypy;
	private String sex;
	private String zzmm;
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
	public String getHypy() {
		return hypy;
	}
	public void setHypy(String hypy) {
		this.hypy = hypy;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getZzmm() {
		return zzmm;
	}
	public void setZzmm(String zzmm) {
		this.zzmm = zzmm;
	}
	
	
}
