package bookerp.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录信息帮助类
 * @author Administrator
 *
 */
public class UserVo implements Serializable{

	private Integer uid; 
	private String xm;
	private String loginName;
	private Integer[] posIds;
	private String[] posNames;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Integer[] getPosIds() {
		return posIds;
	}
	public void setPosIds(Integer[] posIds) {
		this.posIds = posIds;
	}
	public String[] getPosNames() {
		return posNames;
	}
	public void setPosNames(String[] posNames) {
		this.posNames = posNames;
	}
	
	
	
}
