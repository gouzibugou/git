package bookerp.service.sys.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookerp.dao.sys.PostionDao;
import bookerp.entity.Postion;
import bookerp.service.sys.PostionService;


/**
 * ְλҵ����
 * @author Administrator
 *
 */
@Service("postionService")
public class PostionServiceImpl implements PostionService {

	private static Log logger = LogFactory.getLog(PostionServiceImpl.class);
	
	@Resource(name="postionDao")
	private PostionDao postionDao;

	
	@Transactional(readOnly=true)
	public List<Postion> findAll() {
		 return postionDao.findAll();
	}


	@Transactional
	public void savePos(Integer posId, String posName) {
		 try {
			postionDao.savePos(posId,posName);
		
		} catch (Exception e) {
			throw e;
		}
	}


	@Transactional(readOnly=true)
	public int checkPostionUnique(Integer posId) {
		try {
		   Postion p	= postionDao.findPostionByPosId(posId);
		   if(p==null){
			   return 0;
		   }
		   return 1;
		} catch (Exception e) {
			throw e;
		}
	}


	@Transactional(readOnly=true)
	public List<Map<String, Object>> queryMenuByPid(String pid) {
		return postionDao.queryMenuByPid(pid);
	}


	@Transactional(readOnly=true)
	public List<String[]> queryAllMenu() {
		
		return postionDao.queryAllMenu();
	}


	@Transactional(readOnly=true)
	public List<String[]> findAll(int i) {
		return postionDao.findAll(i);
	}
	
	
	
	
	
}
