package bookerp.service.sys.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookerp.dao.sys.UserDao;
import bookerp.entity.Postion;
import bookerp.entity.SysMenu;
import bookerp.entity.SysUser;
import bookerp.service.sys.UserService;
import bookerp.vo.PageBean;
import bookerp.vo.UserVo;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource(name="userDao")
	private UserDao userDao;

	@Transactional(readOnly=true)
	public PageBean queryUserForPage(Map<String, Object> params,
			Integer pageNo, Integer rows) {
		return userDao.queryUserForPage(params, pageNo, rows);
	}

	@Transactional(readOnly=true)
	public List<Map<String,Object>> findUserPosAndpos(String uid) {
		return userDao.findUserPosAndpos(uid);
	}

	@Transactional
	public void saveUserGrantPosInfo(String uid, String[] pids) {
		userDao.saveUserGrantPosInfo(uid,pids);
	}

	@Transactional(readOnly=true)
	public SysUser findUserByLNamePwd(String loginName, String loginPwd) {
		return userDao.findUserByLNamePwd(loginName, loginPwd);
	}

	@Transactional(readOnly=true)
	public List<String> findUserPosAndPos2(String uid) {
		return userDao.findUserPosAndPos2(uid);
	}

	@Transactional(readOnly=true)
	public List<String[]> findUserPosByUid(Integer uid) {
		 
		return userDao.findUserPosByUid(uid);
	}

	@Transactional(readOnly=true)
	public List<SysMenu> findMenuByUid(UserVo u) {
		return userDao.findMenuByUid(u);
	}
	
	
	
}
