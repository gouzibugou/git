package bookerp.service.sys;

import java.util.List;
import java.util.Map;

import bookerp.entity.Postion;
import bookerp.entity.SysMenu;
import bookerp.entity.SysUser;
import bookerp.vo.PageBean;
import bookerp.vo.UserVo;

public interface UserService {
	
	/**
	 * 用户分页查询
	 * @param params
	 * @param pageNo
	 * @param rows
	 * @return
	 */
	public PageBean queryUserForPage(Map<String,Object> params,Integer pageNo,Integer rows);

	/**
	 * 获取用户已拥有职位及职位列表
	 * @param uid
	 * @return
	 */
	public List<Map<String,Object>> findUserPosAndpos(String uid);

	/**
	 * 保存用户授予职位信息
	 * @param uid
	 * @param pids
	 */
	public void saveUserGrantPosInfo(String uid, String[] pids);

	/**
	 * 登录功能
	 * @param loginName
	 * @param loginPwd
	 * @return
	 */
	public SysUser findUserByLNamePwd(String loginName, String loginPwd);

	/**
	 * 根据用户ID，查询出职位ID列表
	 * @param uid
	 * @return
	 */
	public List<String> findUserPosAndPos2(String uid);

	/**
	 * 根据用户ID，查询出职位列表信息
	 * @param uid
	 * @return
	 */
	public List<String[]> findUserPosByUid(Integer uid);

	/**
	 * 查询用户拥有的菜单
	 * @param u
	 * @return
	 */
	public List<SysMenu> findMenuByUid(UserVo u);
}
