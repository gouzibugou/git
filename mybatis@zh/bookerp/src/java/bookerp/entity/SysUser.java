package bookerp.entity;

import java.io.Serializable;
import java.util.Date;
 

public class SysUser implements Serializable{

	private Integer uid;
	private Integer uid2;
	private String xm;
	private Integer zt;
	private Date cjsj;
	private String bz;
	private String loginName;
	
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Integer getUid2() {
		return uid2;
	}
	public void setUid2(Integer uid2) {
		this.uid2 = uid2;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
	public Integer getZt() {
		return zt;
	}
	public void setZt(Integer zt) {
		this.zt = zt;
	}
	public Date getCjsj() {
		return cjsj;
	}
	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	
	
}
