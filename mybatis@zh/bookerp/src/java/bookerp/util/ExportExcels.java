package bookerp.util;

import java.io.OutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportExcels {

	/**
	 * 导出数据
	 * @param outputStream
	 * @param list
	 * @param title
	 */
	public static void exportData(OutputStream outputStream,
			List<String[]> list, String[] title) {
		try {
			//创建workbook
			Workbook book = new XSSFWorkbook();
			//创建sheet
			Sheet sheet = book.createSheet();
			//创建标题行
			Row row = sheet.createRow(0);
			Cell cell;
			int ct = title.length;
			for (int i = 0; i < ct; i++) {
				cell = row.createCell(i);
				cell.setCellValue(title[i]);
			}
			//生成数据行
			if(list!=null){
				int c  = list.size();
				String arr[] = null;
				for (int i = 0; i < c; i++) {
					//获取数据
					arr = list.get(i);
					//创建行
					row = sheet.createRow(i+1);	
					//创建列
					for (int j = 0; j < ct; j++) {
						cell = row.createCell(j);
						cell.setCellValue(arr[j]);
					}
				}
			}
			//关闭资源
			book.write(outputStream);
			book.close();
			outputStream.flush();
			outputStream.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
