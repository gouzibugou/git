package bookerp.dao.sys.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import bookerp.dao.sys.PostionDao;
import bookerp.entity.Postion;
import bookerp.mapper.sys.PostionMapper;

@Repository("postionDao")
public class PostionDaoImpl implements PostionDao {

	private static Log logger = LogFactory.getLog(PostionDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	

	public List<Postion> findAll() {		
		PostionMapper pm = sqlSessionTemplate.getMapper(PostionMapper.class);
		return pm.findAll();
		/*String sql = "select * from T_S_POSTION ";

		return jdbcTemplate.query(sql, new RowMapper<Postion>() {
			@Override
			public Postion mapRow(ResultSet rs, int i) throws SQLException {
				if (rs != null) {
					Postion p = new Postion();
					p.setPosId(rs.getInt("pos_id"));
					p.setPosName(rs.getString("pos_name"));
					p.setStatus(rs.getInt("status"));
					return p;
				}
				return null;
			}
		});*/
	}

	@Override
	public void savePos(Integer posId, String posName) {
		try {
			String sql = "insert into T_S_POSTION" + "(pos_id,pos_name,status)"
					+ "values(?,?,0)";
			jdbcTemplate.update(sql, new Object[] { posId, posName });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Postion findPostionByPosId(Integer posId) {
		try {
			String sql = "select * from T_S_POSTION" + " where pos_id = ? ";
			List<Postion> list = jdbcTemplate.query(sql,
					new Object[] { posId }, new RowMapper<Postion>() {
						@Override
						public Postion mapRow(ResultSet rs, int i)
								throws SQLException {
							if (rs != null) {
								Postion p = new Postion();
								p.setPosId(rs.getInt("pos_id"));
								p.setPosName(rs.getString("pos_name"));
								p.setStatus(rs.getInt("status"));
								return p;
							}
							return null;
						}
					});
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Map<String, Object>> queryMenuByPid(String pid) {
		try {
			String sql = "SELECT m.mid as id,m.cdmc as text,";
			sql += "case m.is_leaf when 0 then 'closed' else 'open' end as state ";
			sql += "from t_s_menu m where m.p_id = ?";

			return jdbcTemplate.queryForList(sql, new Object[] { pid });

		} catch (Exception e) {
			throw e;
		}
	}

	public List<String[]> queryAllMenu() {
		try {
			String sql = "select mid,p_id as pid,cdmc from t_s_menu ";
			return jdbcTemplate.query(sql, new RowMapper<String[]>() {
				public String[] mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					if (rs != null) {
						String[] str = new String[3];
						str[0] = rs.getString("MID");
						str[1] = rs.getString("PID");
						str[2] = rs.getString("CDMC");
						return str;
					}
					return null;
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String[]> findAll(int i) {
		String sql = "select * from T_S_POSTION ";
		
		return jdbcTemplate.query(sql, new RowMapper<String[]>() {
			@Override
			public String[] mapRow(ResultSet rs, int i) throws SQLException {
				if (rs != null) {
					 String[] str = new String[3];
					 str[0] = rs.getInt("POS_ID")+"";
					 str[1] = rs.getString("POS_NAME")+"";
					 str[2] = rs.getInt("STATUS")==0?"可用":"不可用";
					 return str;
				}
				return null;
			}
		});
	}
}
