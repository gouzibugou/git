package bookerp.dao.sys;

import java.util.List;
import java.util.Map;

import bookerp.entity.Postion;
import bookerp.entity.SysMenu;
import bookerp.entity.SysUser;
import bookerp.vo.PageBean;
import bookerp.vo.UserVo;

public interface UserDao {
	public PageBean queryUserForPage(Map<String,Object> params,Integer pageNo,Integer rows);

	public List<Map<String,Object>> findUserPosAndpos(String uid);

	public void saveUserGrantPosInfo(String uid, String[] pids);

	
	public SysUser findUserByLNamePwd(String loginName, String loginPwd);

	public List<String> findUserPosAndPos2(String uid);

	public List<String[]> findUserPosByUid(Integer uid);

	public List<SysMenu> findMenuByUid(UserVo u);
}
