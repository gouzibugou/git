package bookerp.dao.sys.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import bookerp.dao.sys.UserDao;
import bookerp.entity.Postion;
import bookerp.entity.SysMenu;
import bookerp.entity.SysUser;
import bookerp.vo.PageBean;
import bookerp.vo.UserVo;

/**
 * 用户数据处理
 * 
 * @author Administrator
 *
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao {

	private static Log logger = LogFactory.getLog(UserDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * 分页方法
	 * 
	 * @param params
	 * @param pageNo
	 * @param rows
	 * @return
	 */
	public PageBean queryUserForPage(Map<String, Object> params,
			Integer pageNo, Integer rows) {
		PageBean pb = new PageBean();
		try {
			// 查询总条数
			String sql = "select count(*) from t_s_user where 1=1 ";
			// 查询参数组合
			String paramSQL = getParamSQL(params);
			// 执行查询总条数
			Integer total = jdbcTemplate.queryForObject(sql + paramSQL,
					Integer.class);

			// 如果有值查询数据列表
			if (total != null && total > 0) {
				pb.setTotal(total);
				sql = "select * from t_s_user where 1=1 ";
				sql += paramSQL + " order by uid,cjsj desc ";
				// 执行查询
				List<SysUser> list = jdbcTemplate.query(sql,
						new RowMapper<SysUser>() {

							public SysUser mapRow(ResultSet rs, int idx)
									throws SQLException {
								if (rs != null) {
									SysUser u = new SysUser();
									u.setUid(rs.getInt("UID"));
									u.setUid2(rs.getInt("UID"));
									u.setXm(rs.getString("XM"));
									u.setCjsj(rs.getDate("CJSJ"));
									u.setZt(rs.getInt("ZT"));
									u.setBz(rs.getString("BZ"));
									return u;
								}
								return null;
							}

						});
				pb.setRows(list);
			}
		} catch (Exception e) {
			throw e;
		}
		return pb;
	}

	/**
	 * 处理查询参数
	 * 
	 * @param params
	 * @return
	 */
	private String getParamSQL(Map<String, Object> params) {

		return "";
	}

	@Override
	public List<Map<String, Object>> findUserPosAndpos(String uid) {
		try {
			String sql = "select p.pos_id as posId,p.pos_id as posId2,p.pos_name as posName,";
			sql += " A.pos_id as pid from t_s_postion p ";
			sql += " LEFT join (  select r.pos_id";
			sql += " from t_s_user_postion_ref r  ";
			sql += " where r.uid = ? ) A on p.pos_id = A.pos_id ";
			logger.debug(sql);
			List<Map<String, Object>> list = jdbcTemplate.query(sql,
					new Object[] { Integer.parseInt(uid) },
					new RowMapper<Map<String, Object>>() {

						public Map<String, Object> mapRow(ResultSet rs,
								int rowNum) throws SQLException {
							if (rs != null) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("posId", rs.getInt("posId"));
								map.put("posId2", rs.getInt("posId2"));
								map.put("posName", rs.getString("posName"));
								// map.put("pid",rs.getInt("pid"));
								map.put("pid", rs.getLong("pid"));
								return map;
							}
							return null;
						}

					});
			return list;
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public void saveUserGrantPosInfo(String uid, String[] pids) {
		try {
			if (uid == null || "".equals(uid)) {
				throw new NullPointerException("授权用户为空");
			}
			if (pids == null) {
				throw new NullPointerException("授权职位为空");
			}
			// 先删除用户原有职位信息
			String sql = "delete from t_s_user_postion_ref where uid = ?";
			jdbcTemplate.update(sql, new Object[] { Integer.parseInt(uid) });
			// 保存新的信息
			sql = "insert into t_s_user_postion_ref(uid,pos_id)values(?,?)";
			for (int i = 0; i < pids.length; i++) {
				jdbcTemplate.update(sql, new Object[] { Integer.parseInt(uid),
						Integer.parseInt(pids[i]) });
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public SysUser findUserByLNamePwd(String loginName, String loginPwd) {
		try {
			String sql = "select uid,xm,zt,login_name from t_s_user where zt=0"
					+ " and login_name=? and mm=?";
			List<SysUser> list = jdbcTemplate.query(sql, new String[] {
					loginName, loginPwd }, new RowMapper<SysUser>() {
				@Override
				public SysUser mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					if (rs != null) {
						SysUser u = new SysUser();
						u.setUid(rs.getInt("UID"));
						u.setXm(rs.getString("XM"));
						u.setZt(rs.getInt("ZT"));
						u.setLoginName(rs.getString("LOGIN_NAME"));
						return u;
					}
					return null;
				}
			});
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<String> findUserPosAndPos2(String uid) {
		try {
			String sql = "select r.pos_id as posId from "
					+ "t_s_user_postion_ref r where r.uid = ?";
			return jdbcTemplate.queryForList(sql, String.class, uid);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String[]> findUserPosByUid(Integer uid) {
		try {
			String sql = "select p.pos_id,p.pos_name from t_s_postion p ";
			sql += " left join t_s_user_postion_ref r on p.pos_id = r.pos_id ";
			sql += " where r.uid = ?";
			return jdbcTemplate.query(sql, new RowMapper<String[]>() {
				@Override
				public String[] mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					if (rs != null) {
						String[] str = new String[2];
						str[0] = rs.getInt("pos_id") + "";
						str[1] = rs.getString("pos_name");
						return str;
					}
					return null;
				}
			}, uid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<SysMenu> findMenuByUid(UserVo u) {
		try {
			String sql = "select m.mid,m.p_id as pid,";
			sql += " m.cdmc as mc,m.is_leaf as isLeaf,";
			sql += " m.url as url,m.level as level from t_s_menu m ";
			sql += " LEFT JOIN t_s_postion_menu_ref pm on m.mid = pm.MENU_ID";
			sql += " LEFT JOIN t_s_user_postion_ref up on pm.POS_ID = up.pos_id";
			sql += " WHERE up.uid = ?";
			return jdbcTemplate.query(sql,new RowMapper<SysMenu>(){

				 
				public SysMenu mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					if(rs!=null){
						SysMenu m = new SysMenu();
						m.setMc(rs.getString("MC"));
						m.setMid(rs.getString("MID"));
						m.setPid(rs.getString("PID"));
						m.setLevel(rs.getInt("LEVEL"));
						m.setUrl(rs.getString("URL"));
						m.setIsLeaf(rs.getInt("ISLEAF"));
						return m;
					}
					return null;
				}
				
			},u.getUid());	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
