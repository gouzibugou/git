package com.test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bookerp.entity.Postion;
import com.bookerp.mapper.PostionMapper;

public class TestMain2 {
	public static void main(String[] args) {
		try {
			
			String cfg = "mybatis-cfg.xml";
			//获取配置文件
			InputStream input = Resources.getResourceAsStream(cfg);
			//创建sqlSessionFactory
			SqlSessionFactory sessionFactory = 
					new SqlSessionFactoryBuilder().build(input);
			//创建sqlSession
			SqlSession session = sessionFactory.openSession();			
			 
			Map<String,Object> map = new HashMap<>();
			map.put("pid",6);
			map.put("pname","李");
			map.put("status",1);
			//map.put("ids",new Integer[]{1,2,3,4});
			//map.put("flag","不可用");
			PostionMapper pos = session.
					getMapper(PostionMapper.class);
			
			//分页
			//第一个参数是开始位置，第二个取数据条数
			RowBounds rb = new RowBounds(0,2);
			int c = pos.findPosByMap5c(map);
			System.out.println(c);
			
			
			//List<Postion> list  = pos.findPosByMap(map);
			
			List<Postion> list  = pos.findPosByMap5(rb,map);
			for (Postion postion : list) {
				System.out.print(postion.getPosId()+"\t");
				System.out.print(postion.getPosName()+"\t");
				System.out.print(postion.getStatus()+"\n");
			}
			
			//修改
//			int a = pos.updatePos(map);
//			System.out.println(a);
//			session.commit();
			
	
			 
			
			session.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
