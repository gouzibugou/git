package com.test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bookerp.entity.Postion;
import com.bookerp.mapper.PostionMapper;

public class TestMain {
	public static void main(String[] args) {
		try {
			
			String cfg = "mybatis-cfg.xml";
			//获取配置文件
			InputStream input = Resources.getResourceAsStream(cfg);
			//创建sqlSessionFactory
			SqlSessionFactory sessionFactory = 
					new SqlSessionFactoryBuilder().build(input);
			//创建sqlSession
			SqlSession session = sessionFactory.openSession();			
			//session
			//查询
			//List list = session.selectList("com.bookerp.mapper.PostionMapper.findAll");
			//
			//System.out.println(list.size());
			//添加
			Map<String,Object> map = new HashMap<>();
			map.put("pid",6);
			map.put("pname","测试职位");
			map.put("status",0);
			//session.insert("com.bookerp.mapper.PostionMapper",map);
			//删除
			//session.delete("com.bookerp.mapper.PostionMapper", 5);
			
			//2
			PostionMapper pos = session.getMapper(PostionMapper.class);
			//pos.addPostion(map);
			//pos.updatePostion(map);
			
			//List list = session.selectList("com.bookerp.mapper.PostionMapper.findById",5);
			//Map map2 = session.selectMap("com.bookerp.mapper.PostionMapper.findById","java工程师");
			//List<Postion> listp = pos.findByEntity(); 
			//Postion p = pos.findEntityById(5);
			List listall = pos.findAll();
			//session.commit();
			session.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
