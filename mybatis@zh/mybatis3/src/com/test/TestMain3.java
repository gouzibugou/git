package com.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bookerp.entity.Clothes;
import com.bookerp.entity.Postion;
import com.bookerp.mapper.ClothesMapper;
import com.bookerp.mapper.PostionMapper;

public class TestMain3 {
	public static void main(String[] args) {
		try {
			
			String cfg = "mybatis-cfg.xml";
			//获取配置文件
			InputStream input = Resources.getResourceAsStream(cfg);
			//创建sqlSessionFactory
			SqlSessionFactory sessionFactory = 
					new SqlSessionFactoryBuilder().build(input);
			//创建sqlSession
			SqlSession session = sessionFactory.openSession();			
			ClothesMapper mapper = session.getMapper(ClothesMapper.class);
			Map<String,Object> map = new HashMap<String, Object>();
			 
			//新增
			//map.put("bh","FDS0001");
			//map.put("sjsj",new Date());
			//map.put("jg", 1280);
			//mapper.insertClothes(map);		
			
			//修改
			//mapper.updateClothes(map);			
			//删除
			//map.put("ids",new String[]{"S002","S003"});
			//mapper.deleteClothes(map);			
			mapper.deleteClothes2(new String[]{"S002","S003"});
			
			//查询
//			Calendar c = Calendar.getInstance();
//			c.set(Calendar.MONTH,-3);
//			map.put("startDate",c.getTime());
//			map.put("endDate",new Date());
//			int a = mapper.countClothdesMap(map);
//			System.out.println(a);
//			List<Clothes> list = mapper.listClothdesMap(new RowBounds(0, 10),map);
//			System.out.println(list);
			
			//导出	 
			
			//List<Clothes> list = mapper.listClothdesMap(map);
			//exportData(list);
			
			
			session.commit();
			session.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 数据导出
	 * @param list
	 */
	private static void exportData(List<Clothes> list) {
		try {
			File file = new File("D:/temp/test.xlsx");
			if(!file.exists()){
				file.createNewFile();
			}
			FileOutputStream out = new FileOutputStream(file);
			
			Workbook book = new XSSFWorkbook();
			Sheet sheet = book.createSheet();
			//创建标题
			Row row = sheet.createRow(0);
			row.createCell(0).setCellValue("编号");
			row.createCell(1).setCellValue("品牌");
			row.createCell(2).setCellValue("颜色");
			row.createCell(3).setCellValue("尺寸");
			row.createCell(4).setCellValue("材料");
			row.createCell(5).setCellValue("价格");
			row.createCell(6).setCellValue("制造商");
			row.createCell(7).setCellValue("上架日期");
			row.createCell(8).setCellValue("备注");
			Cell cell = null;
			//添加数据
			if(list!=null){
				int i=1;
				for (Clothes c: list) {
					row = sheet.createRow(i);
					row.createCell(0).setCellValue(c.getBh());
					row.createCell(1).setCellValue(c.getPp());
					row.createCell(2).setCellValue("颜色");
					row.createCell(3).setCellValue("尺寸");
					row.createCell(4).setCellValue("材料");
					row.createCell(5).setCellValue("价格");
					row.createCell(6).setCellValue("制造商");
					row.createCell(7).setCellValue(c.getSjsj());
					row.createCell(8).setCellValue("备注");
					i++;
				}
			}
			book.write(out);
			book.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	 
}
