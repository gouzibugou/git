package com.bookerp.entity;

import java.util.Date;

public class Clothes {

	private String bh;
	private String ys;
	private Double jg;
	private String cc;
	private String cz;
	private Date sjsj;
	private String pp;
	private String zzs;
	private String bz;
	
	
	public String getBh() {
		return bh;
	}
	public void setBh(String bh) {
		this.bh = bh;
	}
	public String getYs() {
		return ys;
	}
	public void setYs(String ys) {
		this.ys = ys;
	}
	public Double getJg() {
		return jg;
	}
	public void setJg(Double jg) {
		this.jg = jg;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getCz() {
		return cz;
	}
	public void setCz(String cz) {
		this.cz = cz;
	}
	public Date getSjsj() {
		return sjsj;
	}
	public void setSjsj(Date sjsj) {
		this.sjsj = sjsj;
	}
	public String getPp() {
		return pp;
	}
	public void setPp(String pp) {
		this.pp = pp;
	}
	public String getZzs() {
		return zzs;
	}
	public void setZzs(String zzs) {
		this.zzs = zzs;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	
}
