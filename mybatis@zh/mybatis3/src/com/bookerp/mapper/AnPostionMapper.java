package com.bookerp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.session.RowBounds;

import com.bookerp.entity.Postion;
import com.bookerp.mapper.provider.AnPostionMapperProvider;

/**
 * 注解
 * @author Administrator
 *
 */
public interface AnPostionMapper {

	
	@Insert("insert into t_s_postion (pos_id,pos_name,status)values(#{pid},#{pname},#{status})")
	public int insertPos(Map<String,Object> map);
	
	@Select("select * from t_s_postion")
	public List<Map<String,Object>> findAll();
	
	
	
	@Select("select * from t_s_postion")
	@Results({
		@Result(column="pos_id",property="posId"),
		@Result(column="pos_name",property="posName"),
		@Result(column="status",property="status")
	})
	public List<Postion> findAll2();
	
	
	/**
	 * 动态查询
	 * @return
	 */
	@SelectProvider(type=AnPostionMapperProvider.class,method="findPosById")
	public List<Map<String,Object>> findPosById(Map<String,Object> map);
	
}
