package com.bookerp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.bookerp.entity.Postion;

public interface PostionMapper {

	/**
	 * 查询所有
	 * @return
	 */
	public List<Postion> findAll();
	
	/**
	 * 添加
	 * @param map
	 * @return
	 */
	public int addPostion(Map<String,Object> map);
	
	/**
	 * 删除
	 * @param pid
	 * @return
	 */
	public int delPostion(int pid);
	
	/**
	 * 更新
	 * @param map
	 * @return
	 */
	public int updatePostion(Map<String,Object> map);
	
	
	public List<Postion> findByEntity();
	
	public Postion findEntityById(int pid);
	
	/**
	 * 根据参数查询
	 * @param map
	 * @return
	 */
	public List<Postion> findPosByMap(Map<String,Object> map);
	public List<Postion> findPosByMap2(Map<String,Object> map);
	public List<Postion> findPosByMap3(Map<String,Object> map);
	public List<Postion> findPosByMap4(Map<String,Object> map);
	//分页
	public List<Postion> findPosByMap5(RowBounds rb,Map<String,Object> map);
	public int findPosByMap5c(Map<String,Object> map);
	
	
	public int updatePos(Map<String,Object> map);
}
