package com.bookerp.mapper.provider;

import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

/**
 * AnPostionMapper 动态sql语句
 * @author Administrator
 *
 */
public class AnPostionMapperProvider {

	public String findPosById(Map<String,Object> map){
		System.out.println(map);
		String sql = new SQL(){
			{
				SELECT("*");
				FROM("t_s_postion");
				WHERE("pos_id=#{pid}");
				WHERE("pos_name=#{pname}");
				ORDER_BY("status desc");
			}
		}.toString();		
		
		System.out.println(sql);
		return sql;
	}
	
	public String insert(Map<String,Object> map){		 
		String sql = new SQL(){
			{
				INSERT_INTO("t_s_postion");				
				VALUES("pos_id","#{pid}");
			}
		}.toString();		
		
		System.out.println(sql);
		return sql;
	}
	
	
}
