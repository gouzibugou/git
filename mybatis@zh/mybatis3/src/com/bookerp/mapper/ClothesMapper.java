package com.bookerp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.bookerp.entity.Clothes;

public interface ClothesMapper {

	/**
	 * 新增
	 * @param map
	 * @return
	 */
	public int insertClothes(Map<String,Object> map);
	
	/**
	 * 更新
	 * @param map
	 * @return
	 */
	public int updateClothes(Map<String,Object> map);
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	public int deleteClothes(Map<String,Object> map);
	public int deleteClothes2(String[] ids);
	public int deleteClothes3(List<String> ids);
	
	/**
	 * 分页
	 * @param map
	 * @return
	 */
	public int countClothdesMap(Map<String,Object> map);
	public List<Clothes> listClothdesMap(RowBounds rb,Map<String,Object> map);
	public List<Clothes> listClothdesMap(Map<String,Object> map);
	
}
