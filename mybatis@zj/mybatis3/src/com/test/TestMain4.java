package com.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bookerp.entity.Clothes;
import com.bookerp.entity.Postion;
import com.bookerp.mapper.AnPostionMapper;
import com.bookerp.mapper.ClothesMapper;
import com.bookerp.mapper.PostionMapper;

public class TestMain4 {
	public static void main(String[] args) {
		try {
			
			String cfg = "mybatis-cfg.xml";
			//获取配置文件
			InputStream input = Resources.getResourceAsStream(cfg);
			//创建sqlSessionFactory
			SqlSessionFactory sessionFactory = 
					new SqlSessionFactoryBuilder().build(input);
			//创建sqlSession
			SqlSession session = sessionFactory.openSession();			
			//			
			AnPostionMapper apm = session.getMapper(AnPostionMapper.class);
			//
			Map<String,Object> map = new HashMap<String,Object>();
			
			//insert
			//map.put("pid", 10);
			//map.put("pname","服务员");
			//map.put("status", 0);
			//apm.insertPos(map);
			
			//select
			//List<Map<String,Object>> list = apm.findAll();
			//实体类
			//List<Postion> list = apm.findAll2();
			//System.out.println(list);
			
			//动态sql
			map.put("pid",4);
			List<Map<String,Object>> list = apm.findPosById(map);
			System.out.println(list);
			session.commit();
			session.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

 

	 
}
