package bookhome.pojo;

public class ShopCart {
	private Integer ddid;//订单id
	private Integer id;//用户id
	private String spmc;//商品mc
	private Integer spid;//商品id
	private String fhdz;//发货地址
	private String shdz;//收货地址
	private String cjsj;//创建时间
	private Integer zt;//状态 0-购物车 1-订单以支付出库中 2-发货中 3-以接收 4-其他
	private String bz;//备注
	private Integer spsl;//商品数量
	private String wl;//物流
	private String zffs;//支付方式
	private Integer shid;//收货id
	
	public Integer getShid() {
		return shid;
	}
	public void setShid(Integer shid) {
		this.shid = shid;
	}
	public String getWl() {
		return wl;
	}
	public void setWl(String wl) {
		this.wl = wl;
	}
	public String getZffs() {
		return zffs;
	}
	public void setZffs(String zffs) {
		this.zffs = zffs;
	}
	public Integer getDdid() {
		return ddid;
	}
	public void setDdid(Integer ddid) {
		this.ddid = ddid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSpmc() {
		return spmc;
	}
	public void setSpmc(String spmc) {
		this.spmc = spmc;
	}
	public Integer getSpid() {
		return spid;
	}
	public void setSpid(Integer spid) {
		this.spid = spid;
	}
	public String getFhdz() {
		return fhdz;
	}
	public void setFhdz(String fhdz) {
		this.fhdz = fhdz;
	}
	public String getShdz() {
		return shdz;
	}
	public void setShdz(String shdz) {
		this.shdz = shdz;
	}
	public String getCjsj() {
		return cjsj;
	}
	public void setCjsj(String cjsj) {
		this.cjsj = cjsj;
	}
	public Integer getZt() {
		return zt;
	}
	public void setZt(Integer zt) {
		this.zt = zt;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	public Integer getSpsl() {
		return spsl;
	}
	public void setSpsl(Integer spsl) {
		this.spsl = spsl;
	}
	
}
