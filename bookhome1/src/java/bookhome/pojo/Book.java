package bookhome.pojo;

public class Book {
	private Integer bh;//图书编号
	private Integer zlbh;//种类编号
	private String sm;//书名
	private String yjg;//原价格
	private String zhj;//折后价、
	private String zzmc;//作者名称
	private String cbs;//出版社
	private String cbsj;//出版时间
	private String tsxq;//图书闲情
	public Integer getBh() {
		return bh;
	}
	public void setBh(Integer bh) {
		this.bh = bh;
	}
	public Integer getZlbh() {
		return zlbh;
	}
	public void setZlbh(Integer zlbh) {
		this.zlbh = zlbh;
	}
	public String getSm() {
		return sm;
	}
	public void setSm(String sm) {
		this.sm = sm;
	}
	public String getYjg() {
		return yjg;
	}
	public void setYjg(String yjg) {
		this.yjg = yjg;
	}
	public String getZhj() {
		return zhj;
	}
	public void setZhj(String zhj) {
		this.zhj = zhj;
	}
	public String getZzmc() {
		return zzmc;
	}
	public void setZzmc(String zzmc) {
		this.zzmc = zzmc;
	}
	public String getCbs() {
		return cbs;
	}
	public void setCbs(String cbs) {
		this.cbs = cbs;
	}
	public String getCbsj() {
		return cbsj;
	}
	public void setCbsj(String cbsj) {
		this.cbsj = cbsj;
	}
	public String getTsxq() {
		return tsxq;
	}
	public void setTsxq(String tsxq) {
		this.tsxq = tsxq;
	}
	
}
