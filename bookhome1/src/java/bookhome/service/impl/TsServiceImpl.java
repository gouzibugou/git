package bookhome.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import bookhome.dao.TsDao;
import bookhome.pojo.ShopCart;
import bookhome.service.TsService;
import bookhome.vo.Tree;


@Service("tsService")
public class TsServiceImpl implements TsService{
	@Resource(name="tsDao")
	private TsDao tsDao;




	@Override
	public List<Tree> listtree() {
		// TODO Auto-generated method stub
		return tsDao.listtree();
	}


	@Override
	public List<Map<String, Object>> listjrtj() {
		// TODO Auto-generated method stub
		return tsDao.listjrtj();
	}


	@Override
	public List<Map<String, Object>> listts() {
		// TODO Auto-generated method stub
		return tsDao.listts();
	}


	@Override
	public List<Map<String, Object>> selectBookById(int id) {
		// TODO Auto-generated method stub
		return tsDao.selectBookById(id);
	}


	@Override
	public List<Map<String, Object>> combobox() {
		// TODO Auto-generated method stub
		return tsDao.combobox();
	}


	@Override
	public void addShopCart(ShopCart sc) {
		// TODO Auto-generated method stub
		tsDao.addShopCart(sc);
	}


	@Override
	public List<Map<String, Object>> selectShopCartByUid(Integer id) {
		// TODO Auto-generated method stub
		return tsDao.selectShopCartByUid(id);
	}


	@Override
	public List<Map<String, Object>> listShopCarpByDdid(String ddid) {
		// TODO Auto-generated method stub
		return tsDao.listShopCarpByDdid(ddid);
	}


	@Override
	public void updateDd(String ddidd, ShopCart s) {
		// TODO Auto-generated method stub
		tsDao.updateDd(ddidd,s);
	}


	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		tsDao.deleteById(id);
	}
	
	
}	
