package bookhome.service.impl;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import bookhome.dao.UserDao;
import bookhome.pojo.User;
import bookhome.pojo.UserAddress;
import bookhome.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{
	
	 @Resource(name="userDao")
	  private UserDao userDao;
	  

	public void zc(User u) {
		// TODO Auto-generated method stub
		userDao.zc(u);
	}


	@Override
	public User findUserByNamePwd(String username, String password) {
		// TODO Auto-generated method stub
		return userDao.findUserByNamePwd(username,password);
	}


	@Override
	public List<Map<Object, Object>> selectUserById(Integer id) {
		// TODO Auto-generated method stub
		return userDao.selectUserById(id);
	}


	@Override
	public void saveEditUser(User u) {
		// TODO Auto-generated method stub
		userDao.saveEditUser(u);
	}




	@Override
	public List<Map<String, Object>> selectAddressById(Integer id) {
		// TODO Auto-generated method stub
		return userDao.selectAddressById(id);
	}


	@Override
	public void saveAddress(UserAddress uad) {
		// TODO Auto-generated method stub
		userDao.saveAddress(uad);
		
	}


	@Override
	public void updatemm(User u) {
		// TODO Auto-generated method stub
		userDao.updatemm(u);
	}


	@Override
	public void smrz(User u) {
		// TODO Auto-generated method stub
		userDao.smrz(u);
	}


	@Override
	public void zfmm(User u) {
		// TODO Auto-generated method stub
		userDao.zfmm(u);
	}


	@Override
	public List<Map<String, Object>> selectOrder(Integer id) {
		// TODO Auto-generated method stub
		return userDao.selectOrder(id);
	}
	

}
