package bookhome.service;


import java.util.List;
import java.util.Map;

import bookhome.pojo.User;
import bookhome.pojo.UserAddress;


public interface UserService {
	/**
	 * 注册
	 * @param u
	 */
	  public abstract void zc(User u);
	  /**
	   * 登陆
	   * @param username
	   * @param password
	   * @return
	   */
	  public abstract User findUserByNamePwd(String username, String password);
	  /**
	   * 通过id查询用户
	   * @param id
	   * @return
	   */
	public abstract List<Map<Object, Object>> selectUserById(Integer id);
	/**
	 * 通过用户id修改用户
	 * @param u
	 */
	public abstract void saveEditUser(User u);
	/**
	 * 通过用户id查询收货地址
	 * @param id
	 * @return 
	 */
	public abstract List<Map<String, Object>> selectAddressById(Integer id);
	/**
	 * 保存收货地址
	 * @param uad
	 */
	public abstract void saveAddress(UserAddress uad);
	/**
	 * 修改密码
	 * @param u
	 */
	public abstract void updatemm(User u);
	/**
	 * 实名认证
	 * @param u
	 */
	public abstract void smrz(User u);
	/**
	 * 支付密码
	 * @param u
	 */
	public abstract void zfmm(User u);
	/**
	 * 查询订单
	 * @param id
	 * @return
	 */
	public abstract List<Map<String, Object>> selectOrder(Integer id);
}
