
/** 
* @Title: ExportExecls.java 
* @Package bookert.util 
* @Description: TODO(用一句话描述该文件做什么) 
* @author A18ccms A18ccms_gmail_com 
* @date 2018年11月19日 下午3:13:14 
* @version V1.0 
*/ 
 
package bookhome.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author Administrator
 *
 * @ClassName: ExportExecls 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author A18ccms a18ccms_gmail_com 
 * @date 2018年11月19日 下午3:13:14 
 */
public class ExportExecls {

	/**
	 * @param outputStream
	 * @param list
	 * @param bt
	 * @throws IOException 
	 */
	public static void exportData(OutputStream outputStream, List<Map<String, Object>> list, String bt) throws IOException {
		try {
			// TODO Auto-generated method stub
			/*获取标题*/
			String[] bth = bt.split(",");
			/*创建xssworkbook*/
			XSSFWorkbook book = new XSSFWorkbook();
			/*创建sheet*/ 
			XSSFSheet sheet = book.createSheet("书籍仓库");
			/*创建标题行*/
			Row row = null;
			Cell cell = null;
			row = sheet.createRow(0);
			for (int i = 0; i < bth.length; i++) {
				cell = row.createCell(i);
				cell.setCellValue(bth[i]);	
			}
			int sjcd = list.size();
			
			Map<String,Object> m1 = null;
			//获取map的键
			ArrayList<String> list3 = new ArrayList<String>();
			Map<String, Object> map2 = list.get(0);
			Set<String> keys = map2.keySet();
		
			Iterator<String> iterator = keys.iterator();
			while(iterator.hasNext()){
				String key = iterator.next();
				list3.add(key);
			}
			/*写出到excel表格*/
			for (int i = 0; i < sjcd; i++) {
				row = sheet.createRow(i+1);
				m1 = list.get(i);
				for (int j = 0; j < bth.length; j++) {
					cell = row.createCell(j);
					String a = m1.get(list3.get(j))==null?"null":m1.get(list3.get(j)).toString();
					cell.setCellValue(a);
				}
				
				
			}
			
			
			book.write(outputStream);
			book.close();
			outputStream.flush();
			outputStream.close();
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
}
