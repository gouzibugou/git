package bookhome.dao;

import java.util.List;
import java.util.Map;

import bookhome.pojo.ShopCart;
import bookhome.vo.Tree;


public interface TsDao {

	List<Tree> listtree();

	List<Map<String, Object>> listjrtj();

	List<Map<String, Object>> listts();

	List<Map<String, Object>> selectBookById(int id);

	List<Map<String, Object>> combobox();

	void addShopCart(ShopCart sc);

	List<Map<String, Object>> selectShopCartByUid(Integer id);

	List<Map<String, Object>> listShopCarpByDdid(String ddid);

	void updateDd(String ddidd, ShopCart s);

	void deleteById(Integer id);

}
