package bookhome.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bookhome.dao.UserDao;
import bookhome.mapper.sys.UserMapper;
import bookhome.pojo.User;
import bookhome.pojo.UserAddress;

@Repository("userDao")
public class UserDaoImpl
  implements UserDao
{
  private static Log logger = LogFactory.getLog(UserDaoImpl.class);
  @Autowired
  private SqlSessionTemplate sqlSessionTemplate;


@Override
public void zc(User u) {
	// TODO Auto-generated method stub
	 try {
		 UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	 	   mapper.zc(u);
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
}


@Override
public User findUserByNamePwd(String username, String password) {
	// TODO Auto-generated method stub
	try {
		 UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
		 Map<String,Object> map = new HashMap<String,Object>();
		 map.put("username", username);
		 map.put("password", password);
		 
	 	User	u  = mapper.findUserByNamePwd(map);
	 	return u;
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	return null;
}


@Override
public List<Map<Object, Object>> selectUserById(Integer id) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	return mapper.selectUserById(id);
}


@Override
public void saveEditUser(User u) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	mapper.saveEditUser(u);
}


@Override
public List<Map<String, Object>> selectAddressById(Integer id) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	return  mapper.selectAddressById(id);
	
}


@Override
public void saveAddress(UserAddress uad) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	mapper.saveAddress(uad);
	
}


@Override
public void updatemm(User u) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	mapper.updatemm(u);
}


@Override
public void smrz(User u) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	mapper.smrz(u);
}


@Override
public void zfmm(User u) {
	// TODO Auto-generated method stub
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	mapper.zfmm(u);
}


@Override
public List<Map<String, Object>> selectOrder(Integer id) {
	UserMapper mapper = sqlSessionTemplate.getMapper(UserMapper.class);
	   
	return mapper.selectOrder(id);
}




}
