package bookhome.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bookhome.dao.TsDao;
import bookhome.mapper.ts.TsMapper;
import bookhome.pojo.ShopCart;
import bookhome.vo.Tree;



@Repository("tsDao")
public class TsDaoImpl implements TsDao{
	@Autowired
	private SqlSessionTemplate sqlSessiont;


	@Override
	public List<Tree> listtree() {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		return mapper.listtree();
		
		
	}


	@Override
	public List<Map<String, Object>> listjrtj() {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		

		return mapper.listjrtj();
	}


	@Override
	public List<Map<String, Object>> listts() {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		return mapper.listts();
	}


	@Override
	public List<Map<String, Object>> selectBookById(int id) {
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		return mapper.selectBookById(id);
	}


	@Override
	public List<Map<String, Object>> combobox() {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		return mapper.combobox();
	}


	@Override
	public void addShopCart(ShopCart sc) {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		 mapper.addShopCart(sc);
	}


	@Override
	public List<Map<String, Object>> selectShopCartByUid(Integer id) {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		return mapper.selectShopCartByUid(id);
		 
	}


	@Override
	public List<Map<String, Object>> listShopCarpByDdid(String ddid) {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		String[] ddid1 = ddid.split(",");
		return mapper.listShopCarpByDdid(ddid1);
	}


	@Override
	public void updateDd(String ddidd, ShopCart s) {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		String[] ddid1 = ddidd.split(",");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ddid", ddid1);
		map.put("s", s);
		mapper.updateDd(map);
	}


	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		TsMapper mapper = sqlSessiont.getMapper(TsMapper.class);
		 mapper.deleteById(id);
		
	}
	
}
