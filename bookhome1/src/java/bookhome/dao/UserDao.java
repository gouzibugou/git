package bookhome.dao;


import java.util.List;
import java.util.Map;

import bookhome.pojo.User;
import bookhome.pojo.UserAddress;


public interface UserDao {

	public abstract void zc(User u);

	public abstract User findUserByNamePwd(String username, String password);

	public abstract List<Map<Object, Object>> selectUserById(Integer id);

	public abstract void saveEditUser(User u);

	public abstract List<Map<String, Object>> selectAddressById(Integer id);

	public abstract void saveAddress(UserAddress uad);

	public abstract void updatemm(User u);

	public abstract void smrz(User u);

	public abstract void zfmm(User u);

	public abstract List<Map<String, Object>> selectOrder(Integer id);
}
