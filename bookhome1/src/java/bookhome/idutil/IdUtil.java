package bookhome.idutil;

import java.util.Random;

public class IdUtil {
	public static Integer getId(){
		//随机生成一个八位的ID
		String id = "";
		Random r = new Random();
		for(int i=0;i<8;i++){
			if(i==0){
				id+=r.nextInt(9)+1;//0-9 1-10 [) 
			}else{
				id+=r.nextInt(10);//0-10 [)
			}
		}
		return Integer.parseInt(id);
	}
	public static void main(String[] args) {
		for (int i = 1; i < 10; i++) {
			System.out.println(getId());
		}
		
		
	}
}
