package bookhome.vo;

import java.util.ArrayList;
import java.util.List;

public class Tree {
	private String CODE;
	private String PCODE;
	private String TNAME;
	private Integer LEVEL2;
	private Integer IS_LEAF;
	private List<Object> array;
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getPCODE() {
		return PCODE;
	}
	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}
	public String getTNAME() {
		return TNAME;
	}
	public void setTNAME(String tNAME) {
		TNAME = tNAME;
	}
	public Integer getLEVEL2() {
		return LEVEL2;
	}
	public void setLEVEL2(Integer lEVEL2) {
		LEVEL2 = lEVEL2;
	}
	public Integer getIS_LEAF() {
		return IS_LEAF;
	}
	public void setIS_LEAF(Integer iS_LEAF) {
		IS_LEAF = iS_LEAF;
	}
	public List<Object> getArray() {
		return array;
	}
	public void setArray(List<Object> array) {
		this.array = array;
	}
	
}
