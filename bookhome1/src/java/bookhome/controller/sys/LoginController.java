package bookhome.controller.sys;

import java.time.LocalDate;








import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.rule.Mode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookhome.controller.BaseController;
import bookhome.idutil.IdUtil;
import bookhome.pojo.User;
import bookhome.service.UserService;

@Controller
public class LoginController
  extends BaseController
{
  private static Log logger = LogFactory.getLog(LoginController.class);
  @Resource(name="userService")
  private UserService userService;
  
  
  
  @RequestMapping({"/login.do"})
  @ResponseBody
  public String login(HttpServletRequest request, String username, String password)
  {
    try
    {
      logger.debug("username:++++++++++++++++++" + username);
      logger.debug("password:++++++++++++++++++" + password);
      
     User user = this.userService.findUserByNamePwd(username, password);
     request.getSession().setAttribute("u",user);
     
     if (user!=null) {
		return "success";
	}
     // logger.debug("===========================================================");
      //logger.debug(user);
      //if (user == null) {
     //   return "用户名或者密码错误";
     // }
     // request.getSession().setAttribute("_USER_SESSION_KEY", user);
    }
    catch (Exception e)
    {
      logger.debug(e);
    }
    return "error";
  }
  
  @RequestMapping({"/zc.do"})
  @ResponseBody
  public String register(HttpServletRequest request,User u)
  {
    try
    {
   Integer id = IdUtil.getId();
   u.setId(id);
   	String now = LocalDate.now()+"";
   	u.setCjsj(now);
      userService.zc(u);
      return "success";
    }
    catch (Exception e)
    {
      logger.debug(e);
    }
    return "error";
  }
  /**
   * 退出
   * @param request
   * @param u
   * @return
   */
  @RequestMapping("/tuichu")
  public String tuichu(HttpServletRequest request,User u)
  {
    try
    {
    	request.getSession().removeAttribute("u");
    	return "redirect:ts/toindex.do";
    	}
    catch (Exception e)
    {
      logger.debug(e);
    }
    return null;
  }
}
