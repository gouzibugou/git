package bookhome.controller.sys;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;










import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookhome.idutil.IdUtil;
import bookhome.pojo.User;
import bookhome.pojo.UserAddress;
import bookhome.service.UserService;

@Controller
public class UserController
{
  private static Log logger = LogFactory.getLog(UserController.class);
  @Resource(name="userService")
  private UserService userService;
  
  @RequestMapping("information")
  public ModelAndView information(Integer id){
	  try {
		ModelAndView mv = new ModelAndView("/person/information");
		List<Map<Object,Object>> list = userService.selectUserById(id);
		Map<Object, Object> map = list.get(0);
		mv.addObject("user", map);
		return mv;
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  
  @RequestMapping("saveEditUser")
  @ResponseBody
  public String saveEditUser(User u){
	  try {
		userService.saveEditUser(u);
		return "success";
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  /**
   * 
   * @param id
   * @return
   */
  @RequestMapping("address")
  public ModelAndView saveEditUser(Integer id){
	  try {
		ModelAndView mv = new ModelAndView("/person/address");  
		List<Map<String,Object>> list = userService.selectAddressById(id);
		mv.addObject("dz", list);
		return mv;
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  /**
   *	去个人中心 
   * @param id
   * @return
   */
  @RequestMapping("index")
  public ModelAndView toindex(Integer id){
	  try {
		ModelAndView mv = new ModelAndView("/person/index"); 
		
		return mv;
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  /**
   * 保存收货地址
   * @param uad
   * @return
   */
  @RequestMapping("saveAddress")
  @ResponseBody
  public String saveAddress(UserAddress uad){
	  try {
		  uad.setShid(IdUtil.getId());
		 userService.saveAddress(uad);
		return "success";
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  /**
   * 修改密码
   * @param u
   * @return
   */
  @RequestMapping("updatemm")
  @ResponseBody
  public String updatemm(User u){
	  try {
		 userService.updatemm(u);
		return "success";
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	return null;
  }
  /**
   * 实名认证
   * @param u
   * @return
   */
  @RequestMapping("smrz")
  @ResponseBody
  public String smrz(User u){
	  try {
		 userService.smrz(u);
		return "success";
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	return null;
  }
  /**
   * 支付密码
   * @param u
   * @return
   */
  @RequestMapping("zfmm")
  @ResponseBody
  public String zfmm(User u){
	  try {
		 userService.zfmm(u);
		return "success";
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  
  /**
   * 订单管理
   * @param u
   * @return
   */
  @RequestMapping("order")
  public ModelAndView selectOrder(Integer id){
	  try {
		  ModelAndView mv = new ModelAndView("/person/order");
		List<Map<String,Object>> list =  userService.selectOrder(id);
		mv.addObject("dd",list);
		return mv;
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	return null;
  }
  
}
