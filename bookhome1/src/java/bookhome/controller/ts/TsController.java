package bookhome.controller.ts;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookhome.idutil.IdUtil;
import bookhome.pojo.ShopCart;
import bookhome.pojo.User;
import bookhome.pojo.UserAddress;
import bookhome.service.TsService;
import bookhome.service.UserService;
import bookhome.vo.Tree;

@Controller
@RequestMapping("/ts")
public class TsController {
	@Resource(name="tsService")
	private TsService tsService;
	@Resource(name="userService")
	private UserService userService;
	private static Log logger = LogFactory.getLog(TsController.class);
	
	/**
	 * 去index页面
	 * @return
	 */
	@RequestMapping("/toindex")
	public ModelAndView toindex(HttpServletRequest re){
		try {
			ModelAndView mv = new ModelAndView("/index");
	
			List<Tree> list = tsService.listtree();
			for (int i = 0; i < list.size(); i++) {
				Tree t = list.get(i);
				List<Object> l = new ArrayList<Object>();
				for (int j = 0; j < list.size(); j++) {
					Tree t2 = list.get(j);
					if (t2.getPCODE().equals(t.getCODE())) {
						l.add(t2);
						t.setArray(l);
					}
				}
			}
			List<Map<String,Object>> jrtj = tsService.listjrtj();
			List<Map<String,Object>> ts= tsService.listts();
			for (int i = 0; i < ts.size(); i++) {
				Map<String, Object> map = ts.get(i);
				List<Object> l = new ArrayList<Object>();
				for (int j = 0; j < ts.size(); j++) {
					Map<String, Object> map2 = ts.get(j);
					if (map2.get("PCODE").equals(map.get("CODE"))) {
						l.add(map2);
						map.put("array", l);
					}
				}
			}
			User u =(User)re.getSession().getAttribute("u");
			if (u!=null) {
				Integer id = u.getId();
				List<Map<String, Object>> shopCart = tsService.selectShopCartByUid(id);
				int size = shopCart.size();
				mv.addObject("size",size);
			}
			logger.debug(ts.toString());
			mv.addObject("listtree",list);
			mv.addObject("jrtj",jrtj);
			mv.addObject("listts",ts);
			
			return mv;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping("/introduction")
	public ModelAndView introduction(int id){
		try {
			ModelAndView mv = new ModelAndView("/introduction");
			List<Map<String,Object>> list = tsService.selectBookById(id);
			mv.addObject("l",list);
			return mv;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 搜索框
	 * @return
	 */
	@RequestMapping("/combobox")
	@ResponseBody
	public String combobox(){
		try {
			List<Map<String,Object>> list = tsService.combobox();
			String ss = JSONArray.fromObject(list).toString();
			return ss;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 去支付页面
	 * @param rq
	 * @param id
	 * @param ddid
	 * @return
	 */
	@RequestMapping("/pay")
	public ModelAndView pay(HttpServletRequest rq,Integer id,String ddid){
		try {
			ModelAndView mv = new ModelAndView("pay");
			List<Map<String,Object>> list =	tsService.listShopCarpByDdid(ddid);
			List<Map<String,Object>> l2 = userService.selectAddressById(id);
			mv.addObject("dd", list);
			mv.addObject("dz",l2);
			return mv;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 去购物车页面
	 * @param id
	 * @return
	 */
	@RequestMapping("/shopcart")
	public ModelAndView shopcart(Integer id){
		try {
			
			ModelAndView mv = new ModelAndView("shopcart");
			List<Map<String,Object>> list = tsService.selectShopCartByUid(id);
			mv.addObject("gwc",list);
			return mv;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加到购物车
	 * @param sc
	 * @return
	 */
	@RequestMapping("/addShopCart")
	@ResponseBody
	public String addShopCart(ShopCart sc){
		try {
			Integer ddid = IdUtil.getId();
			sc.setDdid(ddid);
			String now = LocalDate.now()+"";
			sc.setCjsj(now);
			tsService.addShopCart(sc);
			
			return "success";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 修改订单
	 * @param ddidd
	 * @param sc
	 * @return
	 */
	@RequestMapping("/updatedd")
	@ResponseBody
	public String updatedd(String ddidd,ShopCart sc ){
		try {
			tsService.updateDd(ddidd, sc);
			return "success";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 去成功页面
	 * @param uad
	 * @return
	 */
	@RequestMapping("/toSuccess")
	public ModelAndView addShopCart(UserAddress uad){
		try {
			ModelAndView mv = new ModelAndView("success");
			mv.addObject("uad",uad);
			return mv;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping("deleteById")
	@ResponseBody
	public String deleteById(Integer id){
		try {
			tsService.deleteById(id);
			return "success";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping("topay")
	@ResponseBody
	public String topay(ShopCart sc){
		try {
			Integer ddid = IdUtil.getId();
			sc.setDdid(ddid);
			String now = LocalDate.now()+"";
			sc.setCjsj(now);
			tsService.addShopCart(sc);
			return ddid+"";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
}
