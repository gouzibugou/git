package bookhome.mapper.sys;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import bookhome.pojo.User;
import bookhome.pojo.UserAddress;

public interface UserMapper {
		/**
		 * 注册用户
		 * @param u
		 */
		void zc(User u);
		
		/**
		 * 登陆
		 * @param map
		 * @return
		 */
		User findUserByNamePwd(Map<String, Object> map);
		/**
		 * 通过用户id查询用户
		 * @param id
		 * @return
		 */
		List<Map<Object, Object>> selectUserById(Integer id);
		/**
		 * 通过id
		 * @param u
		 */
		void saveEditUser(User u);
		/**
		 * 通过id查询用户收货地址
		 * @param id
		 * @return
		 */
		List<Map<String, Object>> selectAddressById(Integer id);
		/**
		 * 保存收货地址
		 * @param uad
		 */
		@Insert("insert into t_user_address values(#{id},#{shr},#{sjh},#{shdz},#{xxdz},#{shid})")
		void saveAddress(UserAddress uad);
		/**
		 * 修改密码
		 * @param u
		 */
		void updatemm(User u);
		/**
		 * 实名认证
		 * @param u
		 */
		@Update("update t_user_home set xm = #{name},sfzh = #{sfzh} where id = #{id}")
		void smrz(User u);
		/**
		 * 支付密码
		 * @param u
		 */
		@Update("update t_user_home set zfmm = #{zfmm} where id = #{id}")
		void zfmm(User u);
		/**
		 * 查询订单通过用户id
		 * @param id
		 * @return
		 */
		List<Map<String, Object>> selectOrder(Integer id);
		
		
		
}
