package bookhome.mapper.ts;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.session.RowBounds;

import bookhome.pojo.ShopCart;
import bookhome.vo.Tree;


public interface TsMapper {
	/**
	 * 遍历tree
	 * 
	 * @param pid
	 * @return
	 */
	List<Tree> listtree();
	/**
	 * 查询今日推荐
	 * @return
	 */
	List<Map<String, Object>> listjrtj();
	/**
	 * 遍历图书列表
	 * @return
	 */
	List<Map<String, Object>> listts();
	/**
	 * 通过id查询图书
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> selectBookById(int id);
	/**
	 * 搜索框
	 * @return
	 */
	List<Map<String, Object>> combobox();
	/**
	 * 加入购入车
	 * @param sc
	 */
	void addShopCart(ShopCart sc);
	/**
	 * 通过用户id查询购物车
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> selectShopCartByUid(Integer id);
	
	/**
	 * 通过订单id查询查询
	 * @param ddid1
	 * @return
	 */
	
	
	List<Map<String, Object>> listShopCarpByDdid(String[] ddid1);
	/**
	 * 修改定单
	 * @param s
	 */
	void updateDd(Map map);
//	通过Id删除dd
	@Delete("delete from t_dingdan where ddid = #{id}")
	void deleteById(Integer id);
	
}
