<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	request.setAttribute("ctx", request.getContextPath());
%>
<%@ include file="/common/includePub.jsp"%>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=0">

		<title>个人资料</title>

		<link href="${ctx}/resource/AmazeUI-2.4.2/assets/css/admin.css" rel="stylesheet" type="text/css">
		<link href="${ctx}/resource/AmazeUI-2.4.2/assets/css/amazeui.css" rel="stylesheet" type="text/css">

		<link href="${ctx}/resource/css/personal.css" rel="stylesheet" type="text/css">
		<link href="${ctx}/resource/css/infstyle.css" rel="stylesheet" type="text/css">
		<script src="${ctx}/resource/AmazeUI-2.4.2/assets/js/jquery.min.js" type="text/javascript"></script>
		<script src="${ctx}/resource/AmazeUI-2.4.2/assets/js/amazeui.js" type="text/javascript"></script>
			
	</head>

	<body>
		<!--头 -->
<%@ include file="/common/daohan.jsp" %>
            <div class="nav-table">
					   <div class="long-title"><span class="all-goods">全部分类</span></div>
					   <div class="nav-cont">
							<ul>
								<li class="index"><a href="#">首页</a></li>
                                <li class="qc"><a href="#">闪购</a></li>
                                <li class="qc"><a href="#">限时抢</a></li>
                                <li class="qc"><a href="#">团购</a></li>
                                <li class="qc last"><a href="#">大包装</a></li>
							</ul>
						    <div class="nav-extra">
						    	<i class="am-icon-user-secret am-icon-md nav-user"></i><b></b>我的福利
						    	<i class="am-icon-angle-right" style="padding-left: 10px;"></i>
						    </div>
						</div>
			</div>
			<b class="line"></b>
			<c:if test="${user!=null}">
		<div class="center">
			<div class="col-main">
				<div class="main-wrap">

					<div class="user-info">
						<!--标题 -->
						<div class="am-cf am-padding">
							<div class="am-fl am-cf"><strong class="am-text-danger am-text-lg">个人资料</strong> / <small>Personal&nbsp;information</small></div>
						</div>
						<hr/>

						<!--头像 -->
						<div class="user-infoPic">

							<div class="filePic">
								<input type="file" class="inputPic" allowexts="gif,jpeg,jpg,png,bmp" accept="image/*">
								<img class="am-circle am-img-thumbnail" src="${ctx}/resource/images/getAvatar.do.jpg" alt="" />
							</div>

							<p class="am-form-help">头像</p>

							<div class="info-m">
								<div><b>用户名：<i>${user.XM}</i></b></div>
								<div class="u-level">
									<span class="rank r2">
							             <s class="vip1"></s><a class="classes" href="#">铜牌会员</a>
						            </span>
								</div>
								<div class="u-safety">
									<a href="safety.html">
									 账户安全
									<span class="u-profile"><i class="bc_ee0000" style="width: 60px;" width="0">60分</i></span>
									</a>
								</div>
							</div>
						</div>

						<!--个人信息 -->
						<div class="info-main">
							<form class="am-form am-form-horizontal">

								<div class="am-form-group">
									<label for="user-name2" class="am-form-label">昵称</label>
									<div class="am-form-content">
										<input type="text" id="user-name" value="${user.NC }" placeholder="nickname">

									</div>
								</div>

								<div class="am-form-group">
									<label for="user-name" class="am-form-label">姓名</label>
									<div class="am-form-content">
										<input type="text" id="user-name2" value="${user.XM }" placeholder="name">

									</div>
								</div>

								<div class="am-form-group">
									<label class="am-form-label">性别</label>
									<div class="am-form-content sex">
									<c:if test="${user.XB!=null&&user.XB==1 }">
										<label class="am-radio-inline">
											<input checked="checked" type="radio" name="radio10" value="1" data-am-ucheck> 男
										</label>
									</c:if>
									
									<c:if test="${user.XB==null}">
									<label class="am-radio-inline">
											<input type="radio" name="radio10" value="1" data-am-ucheck> 男
									</label>
									</c:if>
									
									<c:if test="${user.XB!=null&&user.XB==2}">
										<label class="am-radio-inline">
											<input checked="checked" type="radio" name="radio10" value="2" data-am-ucheck> 女
										</label>
									</c:if>
									<c:if test="${user.XB==null}">
									<label class="am-radio-inline">
											<input checked="checked" type="radio" name="radio10" value="2" data-am-ucheck> 女
									</label>
									</c:if>
									<c:if test="${user.XB!=null&&user.XB==3 }">
										<label class="am-radio-inline">
											<input checked="checked" type="radio" name="radio10" value="3" data-am-ucheck> 保密
										</label>
									</c:if>
									<c:if test="${user.XB==null}">
									<label class="am-radio-inline">
											<input type="radio" name="radio10" value="3" data-am-ucheck> 保密
										</label>
										</c:if>
										
									</div>
								</div>

								<div class="am-form-group">
									<label for="user-birth" class="am-form-label">生日</label>
									<div class="am-form-content birth">
											<input value="${user.SR }" id ="sr" type="text" class="am-form-field" style="width: 120px;" placeholder="日历组件" data-am-datepicker readonly required />
									</div>
							
								</div>
								<div class="am-form-group">
									<label for="user-phone" class="am-form-label">电话</label>
									<div class="am-form-content">
										<input value="${user.SJH }" id="user-phone" placeholder="telephonenumber" type="tel">
									</div>
								</div>
								<div class="am-form-group">
									<label for="user-email" class="am-form-label">电子邮件</label>
									<div class="am-form-content">
										<input value="${user.YX }" id="user-email" placeholder="Email" type="email">

									</div>
								</div>
								<div class="info-btn">
									<div onclick="saveEdit()" class="am-btn am-btn-danger">保存修改</div>
									<script type="text/javascript">
										function saveEdit(){
											var uid = "${user.ID}";
											var xb  = $("input[type='radio']:checked").val();
											var nc = $("#user-name").val();
											var sjh = $("#user-phone").val()
											var yx = $("#user-email").val()
											var sr = $("#sr").val();
											console.log("uid"+uid+"xb"+xb+"nc"+nc+"sjh"+sjh+"yx"+yx+"sr"+sr);											
											$.ajax({
												url:"${ctx}/saveEditUser.do",
												data:{
													id:uid,
													xb:xb,
													nc:nc,
													phone:sjh,
													yx:yx,
													sr:sr
												},
												success:function(data){
													
													if(data!=null&&data=="success"){alert("修改成功")}
													
												}
											})
										}
									</script>
								</div>

							</form>
						</div>

					</div>

				</div>
				<!--底部-->
				<div class="footer">
					<div class="footer-hd">
						<p>
							<a href="#">恒望科技</a>
							<b>|</b>
							<a href="#">商城首页</a>
							<b>|</b>
							<a href="#">支付宝</a>
							<b>|</b>
							<a href="#">物流</a>
						</p>
					</div>
					<div class="footer-bd">
						<p>
							<a href="#">关于恒望</a>
							<a href="#">合作伙伴</a>
							<a href="#">联系我们</a>
							<a href="#">网站地图</a>
							<em>© 2015-2025 Hengwang.com 版权所有</em>
						</p>
					</div>
				</div>
			</div>
<%@ include file="/common/menu.jsp" %>
		</div></c:if>

	</body>

</html>