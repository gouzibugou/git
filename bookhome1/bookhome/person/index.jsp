<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setAttribute("ctx", request.getContextPath());
%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=0">

<title>个人中心</title>
<%@ include file="/common/includePub.jsp"%>

<link href="${ctx}/resource/AmazeUI-2.4.2/assets/css/admin.css"
	rel="stylesheet" type="text/css">
<link href="${ctx}/resource/AmazeUI-2.4.2/assets/css/amazeui.css"
	rel="stylesheet" type="text/css">
<link href="${ctx}/resource/css/personal.css" rel="stylesheet"
	type="text/css">
<link href="${ctx}/resource/css/systyle.css" rel="stylesheet"
	type="text/css">
</head>

<body>
	<!--头 -->
	<%@ include file="/common/daohan.jsp"%>
	<div class="nav-table">
		<div class="long-title">
			<span class="all-goods">全部分类</span>
		</div>
		<div class="nav-cont">
			<ul>
				<li class="index"><a href="#">首页</a></li>
				<li class="qc"><a href="#">闪购</a></li>
				<li class="qc"><a href="#">限时抢</a></li>
				<li class="qc"><a href="#">团购</a></li>
				<li class="qc last"><a href="#">大包装</a></li>
			</ul>
			<div class="nav-extra">
				<i class="am-icon-user-secret am-icon-md nav-user"></i><b></b>我的福利 <i
					class="am-icon-angle-right" style="padding-left: 10px;"></i>
			</div>
		</div>
	</div>
	<b class="line"></b>
	<div class="center">
		<div class="col-main">
			<div class="main-wrap">
				<div class="wrap-left">
					<div class="wrap-list">
						<div class="m-user">
							<!--个人信息 -->
							<div class="m-bg"></div>
							<div class="m-userinfo">
								<div class="m-baseinfo">
									<a href="information.jsp"> <img
										src="${ctx}/resource/images/getAvatar.do.jpg">
									</a> <em class="s-name">${u.name}<span class="vip1"></em>
									<div class="s-prestige am-btn am-round">
										</span>会员福利
									</div>
								</div>
								<div class="m-right">
									<div class="m-new">
										<a href="news.jsp"><i class="am-icon-bell-o"></i>消息</a>
									</div>
									<div class="m-address">
										<a href="${ctx}/address.do?id=${u.id}" class="i-trigger">我的收货地址</a>
									</div>
								</div>
							</div>

							<!--个人资产-->
							<div class="m-userproperty">
								<div class="s-bar">
									<i class="s-icon"></i>个人资产
								</div>
								<p class="m-bonus">
									<a href="${ctx}/person/bonus.jsp"> <i><img
											src="${ctx}/resource/images/bonus.png" /></i> <span
										class="m-title">红包</span> <em class="m-num">2</em>
									</a>
								</p>
								<p class="m-coupon">
									<a href="${ctx}/person/coupon.jsp"> <i><img
											src="${ctx}/resource/images/coupon.png" /></i> <span
										class="m-title">优惠券</span> <em class="m-num">2</em>
									</a>
								</p>
								<p class="m-bill">
									<a href="${ctx}/person/bill.jsp"> <i><img
											src="${ctx}/resource/images/wallet.png" /></i> <span
										class="m-title">钱包</span> <em class="m-num">2</em>
									</a>
								</p>
								<p class="m-big">
									<a href="#"> <i><img
											src="${ctx}/resource/images/day-to.png" /></i> <span
										class="m-title">签到有礼</span>
									</a>
								</p>
								<p class="m-big">
									<a href="#"> <i><img
											src="${ctx}/resource/images/72h.png" /></i> <span class="m-title">72小时发货</span>
									</a>
								</p>
							</div>
						</div>
						<div class="box-container-bottom"></div>

						<!--订单 -->
						<div class="m-order">
							<div class="s-bar">
								<i class="s-icon"></i>我的订单 <a class="i-load-more-item-shadow"
									href="order.jsp">全部订单</a>
							</div>
							<ul>
								<li><a href="${ctx}/order.do?id=${u.id}"><i><img
											src="${ctx}/resource/images/pay.png" /></i><span>待付款</span></a></li>
								<li><a href="${ctx}/order.do?id=${u.id}"><i><img
											src="${ctx}/resource/images/send.png" /></i><span>待发货<em
											class="m-num">1</em></span></a></li>
								<li><a href="${ctx}/order.do?id=${u.id}"><i><img
											src="${ctx}/resource/images/receive.png" /></i><span>待收货</span></a></li>
								<li><a href="${ctx}/order.do?id=${u.id}"><i><img
											src="${ctx}/resource/images/comment.png" /></i><span>待评价<em
											class="m-num">3</em></span></a></li>
								<li><a href="${ctx}/person/change.jsp"><i><img
											src="${ctx}/resource/images/refund.png" /></i><span>退换货</span></a></li>
							</ul>
						</div>

					</div>
				</div>
				<div class="wrap-right">

					<!-- 日历-->
					<div class="day-list">
						<div class="s-bar">
							<a class="i-history-trigger s-icon" href="#"></a>我的日历 <a
								class="i-setting-trigger s-icon" data-am-modal="{target: '#doc-modal-1', closeViaDimmer: 0, width: 800, height: 550}" href="#"></a>
						</div>
						<div style="display: none;">
							
						</div>
						<div class="am-modal am-modal-no-btn" tabindex="-1"
							id="doc-modal-1">
							<div class="am-modal-dialog">
							<div class="am-modal-hd">
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
								<div class="am-modal-bd">
								<canvas id="canvas" width="500" height="500" />
								</div>
							</div>
						</div>
						<script type="text/javascript"
							src="${ctx }/resource/js/shizhong.js"></script>

						<div class="s-care s-care-noweather">
								<div class="s-date">
									<em id="ri"></em>
									<span id="xq"></span>
									<span id="ny"></span>
									<script type="text/javascript">
										$(function(){
										var myDate = new Date();
										var year=myDate.getFullYear();
										//获取当前月
										var month=myDate.getMonth()+1;
										var day1 = myDate.getDate();
										$("#ri").text(day1);
										$("#ny").text(year+"."+month)
										console.log(month+"/"+day1+"/"+year);
										console.log("07/17/2014");
										 var day = new Date(Date.parse(month+"/"+day1+"/"+year));   //需要正则转换的则 此处为 ： var day = new Date(Date.parse(date.replace(/-/g, '/')));
										 var today = new Array('星期日','星期一','星期二','星期三','星期四','星期五','星期六');
										 var week = today[day.getDay()];
										console.log(week);
										$("#xq").text(week);
										})
									</script>
								</div>
							</div> 

					</div>
					<!--新品 -->

					<!--热卖推荐 -->

				</div>
			</div>
			<!--底部-->
			<div class="footer">
				<div class="footer-hd">
					<p>
						<a href="#">恒望科技</a> <b>|</b> <a href="#">商城首页</a> <b>|</b> <a
							href="#">支付宝</a> <b>|</b> <a href="#">物流</a>
					</p>
				</div>
				<div class="footer-bd">
					<p>
						<a href="#">关于恒望</a> <a href="#">合作伙伴</a> <a href="#">联系我们</a> <a
							href="#">网站地图</a> <em>© 2015-2025 Hengwang.com 版权所有</em>
					</p>
				</div>
			</div>

		</div>

		<%@ include file="/common/menu.jsp"%>
	</div>
</body>

</html>