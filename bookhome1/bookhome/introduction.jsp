<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("ctx", request.getContextPath());
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/common/includePub.jsp"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品詳情</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link type="text/css" href="${ctx}/resource/css/optstyle.css" rel="stylesheet" />
<link type="text/css" href="${ctx}/resource/css/style.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/AmazeUI-2.4.2/assets/css/admin.css">
<link href="${ctx}/resource/basic/css/demo.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resource/css/hmstyle.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }/resource/css/skin.css" rel="stylesheet" type="text/css" />
			
<script type="text/javascript" src="${ctx}/resource/basic/js/quick_links.js"></script>
<script type="text/javascript" src="${ctx}/resource/AmazeUI-2.4.2/assets/js/amazeui.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/jquery.imagezoom.min.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/list.js"></script>
		
</head>

<body>
	
	
	 <%@ include file="common/daohan.jsp" %>
	
	<c:if test="${u.id!=null}">
		<input id="uid" type="hidden" value="${u.id}"></input>
	</c:if>
	<c:forEach items="${l}" var="l">
			
				<!--分类-->
			<!-- <div class="nav-table">
					   <div class="long-title"><span class="all-goods">全部分类</span></div>
					   <div class="nav-cont">
							<ul>
								<li class="index"><a href="#">首页</a></li>
                                <li class="qc"><a href="#">闪购</a></li>
                                <li class="qc"><a href="#">限时抢</a></li>
                                <li class="qc"><a href="#">团购</a></li>
                                <li class="qc last"><a href="#">大包装</a></li>
							</ul>
						    <div class="nav-extra">
						    	<i class="am-icon-user-secret am-icon-md nav-user"></i><b></b>我的福利
						    	<i class="am-icon-angle-right" style="padding-left: 10px;"></i>
						    </div>
						</div>
				</div> -->
				<marquee behavior="alternate" loop="5" width="100%" height="40" bgcolor="#f7f7f7" onmouseover="this.stop()" onMouseOut="this.start()"> <span style="font-size: 18px;">${l.TSXQ}</span></marquee>
				 <ol class="am-breadcrumb am-breadcrumb-slash">
					<li><a href="${ctx}/ts/toindex.do">首页</a></li>
					<li><a href="${ctx}/ts/toindex.do">分类</a></li>
					<li class="am-active">${l.PCODE}</li>
				</ol> 
				<script type="text/javascript">
					$(function(){});
					$(window).load(function() {
						$('.flexslider').flexslider({
							animation: "slide",
							start: function(slider) {
								$('body').removeClass('loading');
							}
						});
					});
				</script>
				
				<div class="scoll">
					<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<li>
									<img src="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" title="${l.TNAME}" />
								</li>
								<li>
									<img src="http://localhost:7979/tsimages/tsimages/${l.TP3}.jpg" title="${l.TNAME}" />
								</li>
								<li>
									<img src="http://localhost:7979/tsimages/tsimages/${l.TP4}.jpg" title="${l.TNAME}" />
								</li>
							</ul>
						</div>
					</section>
				</div>

				<!--放大镜-->

				<div class="item-inform">
					<div class="clearfixLeft" id="clearcontent">

						<div class="box">
							<script type="text/javascript">
								$(document).ready(function() {
									$(".jqzoom").imagezoom();
									$("#thumblist li a").click(function() {
										$(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
										$(".jqzoom").attr('src', $(this).find("img").attr("mid"));
										$(".jqzoom").attr('rel', $(this).find("img").attr("big"));
									});
								});
							</script>

							<div class="tb-booth tb-pic tb-s310">
								<a href="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg"><img src="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" alt="细节展示放大镜特效" rel="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" class="jqzoom" /></a>
							</div>
							<ul class="tb-thumb" id="thumblist">
								<li class="tb-selected">
									<div class="tb-pic tb-s40">
									<a href="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg"><img src="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" alt="细节展示放大镜特效" rel="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" class="jqzoom" /></a>
									</div>
								</li>
								<li>
									<div class="tb-pic tb-s40">
										<a href="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg"><img src="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" alt="细节展示放大镜特效" rel="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" class="jqzoom" /></a>
									
									</div>
								</li>
								<li>
									<div class="tb-pic tb-s40">
									<a href="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg"><img src="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" alt="细节展示放大镜特效" rel="http://localhost:7979/tsimages/tsimages/${l.TP1}.jpg" class="jqzoom" /></a>
									</div>
								</li>
							</ul>
						</div>

						<div class="clear"></div>
					</div>

					<div class="clearfixRight">

						<!--规格属性-->
						<!--名称-->
						<div class="tb-detail-hd">
							<h1 id="sm">	
				 ${l.TNAME}
	          </h1><input id="tsbh" type="hidden" value="${l.BH}">
	          	
						</div>
						<div class="tb-detail-list">
							<div class="tb-detail-price">
								
								<li class="price iteminfo_price">
									<dt>促销价</dt>
									<dd><em>¥</em><b class="sys_item_price">${l.ZHJ }</b>  </dd>                                 
								</li>
								<li class="price iteminfo_mktprice">
									<dt>原价</dt>
									<dd><em>¥</em><b class="sys_item_mktprice">${l.YJG }</b></dd>									
								</li>
								
								<div class="clear"></div>
							</div>

							<!--地址-->
							<dl class="iteminfo_parameter freight">
								<dt>配送至</dt>
								<div class="iteminfo_freprice">
									<div class="am-form-content address">
										<select  id="s_province" name="s_province"></select>
										<select  id="s_city" name="s_city" ></select><select  style="" id="s_county" name="s_county"></select>
											
									</div>
								</div>
							</dl>
							<div class="clear"></div>

							<!--销量-->
							<ul class="tm-ind-panel">
								<li class="tm-ind-item tm-ind-sellCount canClick">
									<div class="tm-indcon"><span class="tm-label">月销量</span><span class="tm-count">1015</span></div>
								</li>
								<li class="tm-ind-item tm-ind-sumCount canClick">
									<div class="tm-indcon"><span class="tm-label">累计销量</span><span class="tm-count">6015</span></div>
								</li>
								<li class="tm-ind-item tm-ind-reviewCount canClick tm-line3">
									<div class="tm-indcon"><span class="tm-label">累计评价</span><span class="tm-count">640</span></div>
								</li>
							</ul>
							<div class="clear"></div>

							<!--各种规格-->
							<dl class="iteminfo_parameter sys_item_specpara">
								<dt class="theme-login"><div class="cart-title">可选规格<span class="am-icon-angle-right"></span></div></dt>
								<dd>
									<!--操作页面-->

									<div class="theme-popover-mask"></div>

									<div class="theme-popover">
										<div class="theme-span"></div>
										<div class="theme-poptit">
											<a href="javascript:;" title="关闭" class="close">×</a>
										</div>
										<div class="theme-popbod dform">
											<form class="theme-signin" name="loginform" action="" method="post">

												<div class="theme-signin-left">

													<div class="theme-options">
														<div class="cart-title number">数量</div>
														<dd>
															<input id="min" class="am-btn am-btn-default" name="" type="button" value="-" />
															<input id="text_box" name="" type="text" value="1" style="width:30px;" />
															<input id="add" class="am-btn am-btn-default" name="" type="button" value="+" />
															<span id="Stock" class="tb-hidden">库存<span class="stock">1000</span>件</span>
														</dd>

													</div>
													<div class="clear"></div>

													<div class="btn-op">
														<div class="btn am-btn am-btn-warning">确认</div>
														<div class="btn close am-btn am-btn-warning">取消</div>
													</div>
												</div>
												<div class="theme-signin-right">
													<div class="img-info">
														<img src="${ctx}/resource/images/songzi.jpg" />
													</div>
													<div class="text-info">
														<span class="J_Price price-now">¥39.00</span>
														<span id="Stock" class="tb-hidden">库存<span class="stock">1000</span>件</span>
													</div>
												</div>

											</form>
										</div>
									</div>

								</dd>
							</dl>
							<div class="clear"></div>
							<!--活动	-->
						</div>

						<div class="pay">
							<div class="pay-opt">
							<a href="home.html"><span class="am-icon-home am-icon-fw">首页</span></a>
							<a><span class="am-icon-heart am-icon-fw">收藏</span></a>
							
							</div>
							<li>
								<div class="clearfix tb-btn tb-btn-buy theme-login">
									<a onclick="goumai()" title="点此按钮到下一步确认购买信息" href="#">立即购买</a>
								</div>
							</li>
							<li>
								<div class="clearfix tb-btn tb-btn-basket theme-login">
									<a onclick="gouwuche()" title="加入购物车" href="#"><i></i>加入购物车</a>
								</div>
							</li>
							<form id="frm" method="post"></form>
							
							<script type="text/javascript">
								function goumai(){
									var uid = $("#uid").val();
									if(uid==undefined){
										alert("亲，请先登陆")
										return false;
									}
									var spid = $("#tsbh").val();
									var spmc = $("#sm").text();
									var fhdz = "${l.CBS}";
									var shdz = $("#s_province").val()+$("#s_city").val()+$("#s_county").val();
									var zt = 0;
									var bz = "购物车";
									var spsl = $("#text_box").val();
									console.log(uid+"+"+spid+"+"+spmc+"+"+fhdz+"+"+shdz+"+"+bz+"+"+spsl+"over");
									$.ajax({
										url:"${ctx}/ts/topay.do",
										type:"POST",
										data:{
											id:uid,
											spid:spid,
											spmc:spmc,
											fhdz:fhdz,
											shdz:shdz,
											zt:zt,
											bz:bz,
											spsl:spsl
										},
										success:function(data){
											if(data!=null){
												$("#frm").attr("action","${ctx}/ts/pay.do?id="+uid+"&ddid="+data+"");
												$("#frm").submit();
											}else{
												alert("添加失败")
											}
										}
										
									})
								}
								function gouwuche(){
									var uid = $("#uid").val();
									if(uid==undefined){
										alert("亲，请先登陆")
										return false;
									}
									var spid = $("#tsbh").val();
									var spmc = $("#sm").text();
									var fhdz = "${l.CBS}";
									var shdz = $("#s_province").val()+$("#s_city").val()+$("#s_county").val();
									var zt = 0;
									var bz = "购物车";
									var spsl = $("#text_box").val();
									console.log(uid+"+"+spid+"+"+spmc+"+"+fhdz+"+"+shdz+"+"+bz+"+"+spsl+"over");
									$.ajax({
										url:"${ctx}/ts/addShopCart.do",
										type:"POST",
										data:{
											id:uid,
											spid:spid,
											spmc:spmc,
											fhdz:fhdz,
											shdz:shdz,
											zt:zt,
											bz:bz,
											spsl:spsl
										},
										success:function(data){
											if(data!=null&&data=="success"){
												alert("添加成功！")
											}else{
												alert("添加失败")
											}
										}
										
									})
								}
							</script>
						</div>

					</div>

					<div class="clear"></div>

				</div>

				<div class="clear"></div>
				
							
				<!-- introduce-->

				<div class="introduce">
					<div class="browse">
					</div>
					<div class="introduceMain">
						<div class="am-tabs" data-am-tabs>
							<ul class="am-avg-sm-3 am-tabs-nav am-nav am-nav-tabs">
								<li class="am-active">
									<a href="#">

										<span class="index-needs-dt-txt">宝贝详情</span></a>

								</li>
							</ul>

							<div class="am-tabs-bd">

								<div class="am-tab-panel am-fade am-in am-active">
									<div class="J_Brand">

										<div class="attr-list-hd tm-clear">
											<h4>产品参数：</h4></div>
										<div class="clear"></div>
										<ul id="J_AttrUL">
											<li title="">产品类型:&nbsp;图书</li>
											<li title="">原料产地:&nbsp;${l.CBS}</li>
											<li title="">产地:&nbsp;${l.CBS}</li>
											<li title="">出版时间:&nbsp;${l.CBSJ }</li>
											<li title="">产品标准号:&nbsp;GB/T ${l.CODE }</li>
											<li title="">生产许可证编号：&nbsp;QS4201 1801 ${l.CODE }</li>
											<li title="">储存方法：&nbsp;请放置于常温、阴凉、通风、干燥处保存 </li>
										</ul>
										<div class="clear"></div>
									</div>

									<div class="details">
										<div class="attr-list-hd after-market-hd">
											<h4>商品细节</h4>
										</div>
										<div class="twlistNews">
											<img src="http://localhost:7979/tsimages/tsimages/${l.TP10}.jpg" />
										
											
										</div>
									</div>
									<div class="clear"></div>

								</div>



							</div>

						</div>

						<div class="clear"></div>

			</c:forEach>
			<script type="text/javascript">_init_area();</script>
		
</body>
</html>