<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("ctx", request.getContextPath());
%>

<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/common/includePub.jsp"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="Shortcut Icon" href="${ctx}/resource/image/favicon.ico" />
<title>登陆页面</title>
<style type="text/css">
.td1, .td2 {
	line-height: 20px
}

.td2 {
	width: 280px;
	text-align: right;
	padding-left: 5px;
	padding-top: 25px;
	height: 60px;
}

.state1 {
	color: #aaa;
}

.state2 {
	color: #000;
}

.state3 {
	color: red;
}

.state4 {
	color: green;
	font-size: 20px;
}

html, body {
	border: 0px;
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
}
</style>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="stylesheet" href="${ctx}/resource/css/dlstyle.css" />
</head>

<body>
	<div class="login-boxtitle">
		<a href="home.html"><img alt="logo"
			src="${ctx}/resource/images/logobig.jpg" /></a>
	</div>

	<div class="login-banner">
		<div class="login-main">
			<div class="login-banner-bg">
				<span></span><img src="${ctx}/resource/images/big.jpg" />
			</div>
			<div class="login-box">

				<h3 class="title">登录商城</h3>

				<div class="clear"></div>

				<div class="login-form">
					<form id="frm">
						<div class="user-name">
							<label for="user"><i class="am-icon-user"></i></label> <input
								type="text" name="" id="user" placeholder="邮箱/手机/用户名">
						</div>
						<div class="user-pass">
							<label for="password"><i class="am-icon-lock"></i></label> <input
								type="password" name="" id="password" placeholder="请输入密码">
						</div>
					</form>
				</div>

				<div class="login-links">
					<label for="remember-me"><input id="remember-me"
						type="checkbox">记住密码</label> <a href="${ctx}/register.jsp"
						class="zcnext am-fr am-btn-default">注册</a> <br />
				</div>
				<div class="am-cf">
					<input onclick="denglu()" type="submit" name="" value="登 录"
						class="am-btn am-btn-primary am-btn-sm">
				</div>

			</div>
		</div>
	</div>
	<div class="footer">
		<div class="footer-hd">
			<p>
				<a href="#">恒望科技</a> <b>|</b> <a href="#">商城首页</a> <b>|</b> <a
					href="#">支付宝</a> <b>|</b> <a href="#">物流</a>
			</p>
		</div>
		<div class="footer-bd">
			<p>
				<a href="#">关于恒望</a> <a href="#">合作伙伴</a> <a href="#">联系我们</a> <a
					href="#">网站地图</a> <em>© 2015-2025 Hengwang.com 版权所有</em>
			</p>
		</div>
	</div>
	<script type="text/javascript">
		function denglu(){
			var username = $("#user").val();
			var password =$("#password").val();
			
			$.ajax({
				url:"${ctx}/login.do",
				data:{
					username:username,
					password:password},
				success:function(data){
					if(data!=null&&data=="success"){
						$("#frm").attr("action","${ctx}/ts/toindex.do");
						$("#frm").submit();
					}else{
						alert("账号密码不正确！请检查")
					}
				}
			})
		}
	</script>
</body>
</html>