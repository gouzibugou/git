<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.easyui.min.js"></script>
<aside class="menu">
				<ul style="margin-left: 0px;">
					<li class="person">
						<a href="${ctx}/index.do?id=${u.id}">个人中心</a>
					</li>
					<li class="person">
						<a href="#">个人资料</a><hr>
						<ul>
							<li class="active"> <a href="${ctx}/information.do?id=${u.id}">个人信息</a></li>
							<li class="active"> <a href="${ctx}/person/safety.jsp">安全设置</a></li>
							<li class="active"> <a href="${ctx}/address.do?id=${u.id}">收货地址</a></li>
						</ul>
					</li>
					<li class="person">
						<a href="#">我的交易</a><hr>
						<ul>
							<li class="active"> <a href="${ctx}/order.do?id=${u.id}">订单管理</a></li>
							<li class="active"> <a href="${ctx}/person/change.jsp">退款售后</a></li>
						</ul>
					</li>
					<li class="person">
						<a href="#">我的资产</a><hr>
						<ul>
							<li class="active"> <a href="${ctx}/person/bill.jsp">账单明细</a></li>
						</ul>
					</li>

					<!-- <li class="person">
						<a href="#">我的小窝</a><hr>
						<ul>
							<li class="active"> <a href="collection.jsp">收藏</a></li>
							<li class="active"> <a href="foot.jsp">足迹</a></li>
							<li class="active"> <a href="comment.jsp">评价</a></li>
							<li class="active"> <a href="news.jsp">消息</a></li>
						</ul>
					</li> -->

				</ul>

			</aside>