<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<link rel="stylesheet" type="text/css" href="${ctx }/resource/js/jquery-eayui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="${ctx }/resource/js/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx }/resource/AmazeUI-2.4.2/assets/css/amazeui.min.css"/>

<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.min.js"></script>
<script class="resources library" src="${ctx }/resource/js/area.js" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/ckeditor/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/resource/AmazeUI-2.4.2/assets/js/amazeui.min.js"></script>
<script type="text/javascript" src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
