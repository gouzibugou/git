<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/jquery-eayui/jquery.easyui.min.js"></script>
 <div class="hmtop">
			<!--顶部导航条 -->
			<div class="am-container header">
				<c:if test="${u.name==null}">
				<ul class="message-l">
					<div class="topMessage">
						<div class="menu-hd">
							<a href="${ctx}/login.jsp" target="t_black" class="h">亲，请登录</a>
							<a href="${ctx}/register.jsp" target="t_black">免费注册</a>
						</div>
					</div>
				</ul>
				</c:if>
				<c:if test="${u.name!=null}">
				<ul class="message-l">
					<div class="topMessage">
						<div class="menu-hd">
							<a href="#"  class="h">欢迎${u.name}</a>
							<a href="${ctx}/tuichu.do" class="h">退出</a>
						</div>
					</div>
				</ul>
				</c:if>
				<ul class="message-r">
					<div class="topMessage home">
						<div class="menu-hd"><a href="${ctx}/ts/toindex.do" target="_top" class="h">商城首页</a></div>
					</div>
					<div class="topMessage my-shangcheng">
					<c:if test="${u.name!=null}">
						<div class="menu-hd MyShangcheng"><a href="${ctx}/index.do?id=${u.id}" target="_top"><i class="am-icon-user am-icon-fw"></i>个人中心</a></div>
					</c:if>
					<c:if test="${u.name==null}">
						<div class="menu-hd MyShangcheng"><a href="#" target="_top"><i class="am-icon-user am-icon-fw"></i>登陆后可入个人中心</a></div>
					</c:if>
					</div>
					<div class="topMessage mini-cart">
						<div class="menu-hd"><a id="mc-menu-hd" href="${ctx}/ts/shopcart.do?id=${u.id}" target="_top"><i class="am-icon-shopping-cart  am-icon-fw"></i><span>购物车</span><strong id="J_MiniCartNum" class="h"></strong></a></div>
					</div>
					<div class="topMessage favorite">
						<div class="menu-hd"><a href="#" target="_top"><i class="am-icon-heart am-icon-fw"></i><span>收藏夹</span></a></div>
				</ul>
				</div>

				<!--悬浮搜索框-->

				<div class="nav white">
					<div class="logoBig">
						<li><img src="${ctx}/resource/images/logobig.jpg" /></li>
					</div>

					<div class="search-bar pr">
						<a name="index_none_header_sysc" href="#"></a>
						<form id="frm" method="post"> 
							<input id="searchInput"   type="text" placeholder="搜索" autocomplete="off">
							<script type="text/javascript">
								$(function(){
									$("#searchInput").combobox({
										url:'${ctx}/ts/combobox.do',    
									    valueField:'id',    
									    textField:'text',
									    panelMaxHeight:100,
									    hasDownArrow:false,
									    width:657
									})
								})
								function shousuo(){
									var id = $("#searchInput").val();
									$("#frm").attr("action","${ctx}/ts/introduction.do?id="+id+"");
									$("#frm").submit();
								}
							</script>
							<input id="ai-topsearch" onclick="shousuo()" value="搜索" class="submit am-btn" >
							<style type="text/css">
								#ai-topsearch{
								position: absolute;
								right: 12px;
								top: 20px;
								width: 100px;
								height: 52px;
								}
							</style>
						</form>
					</div>
				</div>

				<div class="clear"></div>
				</div>

	