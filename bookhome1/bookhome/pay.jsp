<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
	request.setAttribute("ctx", request.getContextPath());
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0 ,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>结算页面</title>

<%@ include file="/common/includePub.jsp"%>
		<link href="${ctx}/resource/basic/css/demo.css" rel="stylesheet" type="text/css" />
		<link href="${ctx}/resource/css/cartstyle.css" rel="stylesheet" type="text/css" />

		<link href="${ctx}/resource/css/jsstyle.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="${ctx}/resource/js/address.js"></script>
</head>
<body>

		<!--顶部导航条 -->
		<%@ include file="/common/daohan.jsp"%>
			<div class="clear"></div>
			<div class="concent">
				<!--地址 -->
				<div class="paycont">
					<div class="address">
						<h3>确认收货地址 </h3>
						<div class="control">
						</div>
						<div class="clear"></div>
						<ul class="am-avg-sm-1 am-avg-md-3 am-thumbnails">
							<c:forEach items="${dz}" var="shdz">
							<li class="user-addresslist">
								<span class="new-option-r"><i class="am-icon-check-circle"></i>默认地址</span>
								<p class="new-tit new-p-re">
									<span class="new-txt">${shdz.SHR}</span>
									<span class="new-txt-rd2">${shdz.SHRSJH}</span>
									<input type="hidden" value="${shdz.SHID }">
								</p>
								<div class="new-mu_l2a new-p-re">
									<p class="new-mu_l2cw">
										<span class="title">地址：</span>
										<span class="province">${shdz.SZDZ }</span>
										<span class="street">${shdz.XXDZ }</span>
										
									</p>
								</div>
								
							</li>
							</c:forEach>
						</ul>
					<div class="clear"></div>
				</div>
					<!--物流 -->
					<div id="wl" title="" class="logistics">
						<h3>选择物流方式</h3>
						<ul class="op_express_delivery_hot">
							<li title="圆通"  class="OP_LOG_BTN  "><i class="c-gap-right" style="background-position:0px -468px"></i>圆通<span></span></li>
							<li title="申通"  class="OP_LOG_BTN  "><i class="c-gap-right" style="background-position:0px -1008px"></i>申通<span></span></li>
							<li title="韵达"     class="OP_LOG_BTN  "><i class="c-gap-right" style="background-position:0px -576px"></i>韵达<span></span></li>
							<li title="中通" class="OP_LOG_BTN op_express_delivery_hot_last "><i class="c-gap-right" style="background-position:0px -324px"></i>中通<span></span></li>
							<li title="顺风"  class="OP_LOG_BTN  op_express_delivery_hot_bottom"><i class="c-gap-right" style="background-position:0px -180px"></i>顺丰<span></span></li>
						</ul>
					</div>
					<div class="clear"></div>

					<!--支付方式-->
					<div id="zf" title="" class="logistics">
						<h3>选择支付方式</h3>
						<ul class="pay-list">
							<li title="银行卡" class="pay card"><img src="${ctx}/resource/images/wangyin.jpg" />银联<span></span></li>
							<li title="QQ支付" class="pay qq"><img src="${ctx}/resource/images/weizhifu.jpg" />微信<span></span></li>
							<li title="支付宝" class="pay taobao"><img src="${ctx}/resource/images/zhifubao.jpg" />支付宝<span></span></li>
						</ul>
					</div>
					<div class="clear"></div>
						<script type="text/javascript">
						$(document).ready(function() {							
							$(".user-addresslist").click(function() {
								$(this).addClass("defaultAddr").siblings().removeClass("defaultAddr");
								var span = $(this).children("*").children("span");
								var input = $(this).children("*").children("input");
								var shr = span[0].innerText;
								var shrshj = span[1].innerText;
								var span1 = $(this).children("*").children("p").children("span");
								var shdz = span1[1].innerText+span1[2].innerText;
								var shid = input[0].defaultValue;
								$("#dz").text(shdz);
								$("#shr").text(shr);
								$("#sjh").text(shrshj);
								$("#shid").val(shid);
								
							});
							
							var $ww = $(window).width();
							if($ww>640) {
								$("#doc-modal-1").removeClass("am-modal am-modal-no-btn")
							}
							
						})
					</script>
					<!--订单 -->
					<div class="concent">
						<div id="payTable">
							<h3>确认订单信息</h3>
							<div class="cart-table-th">
								<div class="wp">

									<div class="th th-item">
										<div class="td-inner">商品信息</div>
									</div>
									<div class="th th-price">
										<div class="td-inner">单价</div>
									</div>
									<div class="th th-amount">
										<div class="td-inner">数量</div>
									</div>
									<div class="th th-sum">
										<div class="td-inner">金额</div>
									</div>
									<div class="th th-oplist">
										<div class="td-inner">配送方式</div>
									</div>

								</div>
							</div>
							<div class="clear"></div>

							<tr class="item-list">
								<div class="bundle  bundle-last">
									<c:forEach items="${dd}" var="d">
									<div class="bundle-main">
										<ul class="item-content clearfix">
											<div class="pay-phone">
												<li class="td td-item">
													<div class="item-pic">
														<a href="#" class="J_MakePoint">
															<img src="http://localhost:7979/tsimages/tsimages/${d.TP1}.jpg" class="itempic J_ItemImg"></a>
													</div>
													<div class="item-info">
														<div class="item-basic-info">
															<a href="#" class="item-title J_MakePoint" data-point="tbcart.8.11">${d.SM}</a>
														</div>
													</div>
												</li>
												<li class="td td-info">
													<div class="item-props">
														<span class="sku-line">包装：裸装</span>
													</div>
												</li>
												<li class="td td-price">
													<div class="item-price price-promo-promo">
														<div class="price-content">
															<em class="J_Price price-now">${d.ZHJ}</em>
														</div>
													</div>
												</li>
											</div>
											<li class="td td-amount">
												<div class="amount-wrapper ">
													<div class="item-amount ">
														<span class="phone-title">购买数量</span>
														<div class="sl">
															<input class="text_box" name="ddid" alt="${d.DDID}" type="text" value="${d.SPSL }" style="width:30px;" />
														</div>
													</div>
												</div>
											</li>
											<li class="td td-sum">
												<div class="td-inner">
													<em tabindex="0" id="t_${d.DDID}" name="je" class="J_ItemSum number">${d.SPSL*d.ZHJ }</em>
												</div>
											</li>
											<li class="td td-oplist">
												<div class="td-inner">
													<span class="phone-title">配送方式</span>
													<div class="pay-logis">
														快递<b class="sys_item_freprice">10</b>元
													</div>
												</div>
											</li>

										</ul>
										<div class="clear"></div>
									</div>
									</c:forEach>
									
							</tr>
							<div class="clear"></div>
							</div>

						</div>
							<div class="clear"></div>
							<div class="pay-total">
								<!--留言-->
							<div class="order-extra">
								<div class="order-user-info">
									<div id="holyshit257" class="memo">
										<label>买家留言：</label>
										<input type="text" title="选填,对本次交易的说明（建议填写已经和卖家达成一致的说明）" placeholder="选填,建议填写和卖家达成一致的说明" class="memo-input J_MakePoint c2c-text-default memo-close">
										<div class="msg hidden J-msg">
											<p class="error">最多输入500个字符</p>
										</div>
									</div>
								</div>

							</div>
							<!--优惠券 -->
							<div class="clear"></div>
							</div>
							<!--含运费小计 -->
							<div class="buy-point-discharge ">
								<p class="price g_price ">
									合计（含运费） <span>¥</span><em id="heji"class="pay-sum"></em>
									<script type="text/javascript">
										$(function(){
											$(".OP_LOG_BTN").click(function(){
												 var a = $(this)[0].title;
												 console.log(a);
												 $("#wl").attr("title",a);
												}
											)
											$(".pay").click(function(){
												 var b = $(this)[0].title;
												 console.log(b);
												 $("#zf").attr("title",b);
												}
											)
											
												
											var jearr = $("em[name='je']");
											var arr = jearr.toArray();
											var x = 0;
											for (var i = 0; i < arr.length; i++) {
												var a = arr[i].innerText;
												x+=parseInt(a)+10;
 											}
											$("#heji").text(x);
											$("#J_ActualFee").text(x);
											
										})
									</script>
								</p>
							</div>

							<!--信息 -->
							<div class="order-go clearfix">
								<div class="pay-confirm clearfix">
									<div class="box">
										<div tabindex="0" id="holyshit267" class="realPay"><em class="t">实付款：</em>
											<span class="price g_price ">
                                    <span>¥</span> <em class="style-large-bold-red " id="J_ActualFee"></em>
											</span>
										</div>

										<div id="holyshit268" class="pay-address">

											<p class="buy-footer-address">
												<span class="buy-line-title buy-line-title-type">寄送至：</span>
												<span class="buy--address-detail">
								   				<span id = "dz" class="province"></span>
												</span>
											</p>
											<p class="buy-footer-address">
												<input type="hidden" id="shid">
												<span class="buy-line-title">收货人：</span>
												<span class="buy-address-detail">   
                                         		<span id = "shr"class="buy-user"></span>
												<span id= "sjh" class="buy-phone"></span>
											</span>
											</p>
										</div>
									</div>

									<div id="holyshit269" class="submitOrder">
										<div class="go-btn-wrap">
										<form id="frm"></form>
										
											<a id="J_Go" href="#" onclick="toSuccess()" class="btn-go am-btn am-btn-primary" data-am-modal="{target: '#my-alert'}" tabindex="0" title="点击此按钮，提交订单">提交订单</a>
											<script type="text/javascript">
											
												function toSuccess(){
													var wl = $("#wl")[0].title;//物流
													var zffs = $("#zf")[0].title;//支付方式
													var arr = [];
													var shid = $("#shid")[0].defaultValue;
													var d = $("input[name='ddid']");//订单id
													for (var i = 0; i < d.length; i++) {
													arr.push(d[i].alt);
													}
													console.log(shid);
													var shr = $("#shr").text();//收货人
													var shrsjh = $("#sjh").text();//收货人手机号
													var dz = $("#dz").text();//地址
													var jg = $("#J_ActualFee").text();//总价格
													console.log(shr+shrsjh+dz+jg);
													setTimeout(function(){
														
														$.ajax({
															url:"${ctx}/ts/updatedd.do",
															data:{
																shid:shid,
																wl:wl,
																zffs:zffs,
																ddidd:arr.join(","),
																shr:shr,
																sjh:shrsjh,
																xxdz:dz,
																jg:jg	
																	},
															success:function(data){
																if(data!=null&&data=="success"){
																	alert("支付成功");
																}
															}
															
															
														})
														$("#frm").attr("action","${ctx}/ts/toSuccess.do?shr="+shr+"&sjh="+shrsjh+"&xxdz="+dz+"&jg="+jg);
														$("#frm").attr("method","POST");
														$("#frm").submit();
													}, 3000);
												}
											</script>
											<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
											  <div class="am-modal-dialog">
											    <div class="am-modal-bd">
											      	<img width="200px" height="250px"  src="${ctx}/resource/image/wxzf.png">
											    </div>
											  </div>
											</div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div class="clear"></div>
					</div>
				</div>
				<div class="footer">
					<div class="footer-hd">
						<p>
							<a href="#">恒望科技</a>
							<b>|</b>
							<a href="#">商城首页</a>
							<b>|</b>
							<a href="#">支付宝</a>
							<b>|</b>
							<a href="#">物流</a>
						</p>
					</div>
					<div class="footer-bd">
						<p>
							<a href="#">关于恒望</a>
							<a href="#">合作伙伴</a>
							<a href="#">联系我们</a>
							<a href="#">网站地图</a>
							<em>© 2015-2025 Hengwang.com 版权所有</em>
						</p>
					</div>
				</div>
			</div>
			<div class="clear"></div>
</body>
</html>