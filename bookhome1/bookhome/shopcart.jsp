<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	request.setAttribute("ctx", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>购物车页面</title>
		<%@ include file="/common/includePub.jsp"%>
		<link href="${ctx }/resource/basic/css/demo.css" rel="stylesheet" type="text/css" />
		<link href="${ctx }/resource/css/cartstyle.css" rel="stylesheet" type="text/css" />
		<link href="${ctx }/resource/css/optstyle.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${ctx}/resource/basic/js/quick_links.js"></script>
		<script type="text/javascript" src="${ctx}/resource/AmazeUI-2.4.2/assets/js/amazeui.js"></script>
		<script type="text/javascript" src="${ctx}/resource/js/jquery.imagezoom.min.js"></script>
		<script type="text/javascript" src="${ctx}/resource/js/jquery.flexslider.js"></script>
		<script type="text/javascript" src="${ctx}/resource/js/list.js"></script>
		
</head>
<body>
	<%@ include file="/common/daohan.jsp"%>
	<div class="clear"></div>

			<!--购物车 -->
			<div class="concent">
				<div id="cartTable">
					<div class="cart-table-th">
						<div class="wp">
							<div class="th th-chk">
								<div id="J_SelectAll1" class="select-all J_SelectAll">

								</div>
							</div>
							<div class="th th-item">
								<div class="td-inner">商品信息</div>
							</div>
							<div class="th th-price">
								<div class="td-inner">单价</div>
							</div>
							<div class="th th-amount">
								<div class="td-inner">数量</div>
							</div>
							<div class="th th-sum">
								<div class="td-inner">金额</div>
							</div>
							<div class="th th-op">
								<div class="td-inner">操作</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<c:forEach items="${gwc}" var="gwc">
					<tr class="item-list">
						<div class="bundle  bundle-last ">
							<div class="clear"></div>
							<div class="bundle-main">
								<ul class="item-content clearfix">
									<li class="td td-chk">
										<div class="cart-checkbox ">
											<input onclick="smallcheaked(this)" class="check" id="J_CheckBox_${gwc.DDID}" alt="${gwc.DDID}" name="items[]" value="${gwc.SPSL*gwc.ZHJ}" type="checkbox">
											<label for="J_CheckBox_${gwc.DDID}"></label>
										</div>
									</li>
									<li class="td td-item">
										<div class="item-pic">
											<a href="${ctx}/ts/introduction.do?id=${gwc.BH}" target="_blank" data-title="${gwc.SM}" class="J_MakePoint" data-point="tbcart.8.12">
												<img src="http://localhost:7979/tsimages/tsimages/${gwc.TP1}.jpg" class="itempic J_ItemImg"></a>
										</div>
										<div class="item-info">
											<div class="item-basic-info">
												<a href="${ctx}/ts/introduction.do?id=${gwc.BH}" target="_blank" title="${gwc.SM}" class="item-title J_MakePoint" data-point="tbcart.8.11">${gwc.SM}</a>
											</div>
										</div>
									</li>
									<li class="td td-info">
										<div class="item-props item-props-can">
										</div>
									</li>
									<li class="td td-price">
										<div class="item-price price-promo-promo">
											<div class="price-content">
												<div class="price-line">
													<em class="price-original">${gwc.YJG }</em>
												</div>
												<div class="price-line">
													<em class="J_Price price-now" tabindex="0">${gwc.ZHJ }</em>
												</div>
											</div>
										</div>
									</li>
									<li class="td td-amount">
										<div class="amount-wrapper ">
											<div class="item-amount ">
												<div class="sl">
													<input id="min" class="min am-btn" name="" type="button" value="-" />
													<input id="text_box"  name="" type="text" value="${gwc.SPSL}" style="width:30px;" />
													<input id="add"class="add am-btn" name="" type="button" value="+" />
												</div>
											</div>
										</div>
									</li>
									<li class="td td-sum">
										<div class="td-inner">
											<em tabindex="0" class="J_ItemSum number">${gwc.SPSL*gwc.ZHJ}</em>
											
										</div>
									</li>
									<li class="td td-op">
										<div class="td-inner">
											<a href="javascript:;" onclick="deleteById('${gwc.DDID}')" class="am-btn am-btn-danger">
                								  删除</a>
                								  <script type="text/javascript">
                								  	function deleteById(id){
                								  		alert(id);
                								  		$.ajax({
                								  			url:"${ctx}/ts/deleteById.do",
                								  			data:{
                								  				id :id,
                								  			},
                								  			success:function(data){
                								  				if(data!=null&&data=="success"){
                								  					alert("删除成功！");
                								  					window.location.reload();
                								  				}
                								  				
                								  			}
                								  		})
                								  		
                								  	}
                								  </script>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</tr>
					
					<div class="clear"></div>
					</c:forEach>
				</div>
				<div class="clear"></div>

				<div class="float-bar-wrapper">
					<div id="J_SelectAll2" class="select-all J_SelectAll">
						<div class="cart-checkbox">
							<input onclick="bigcheaked(this)" class="check-all check" id="J_SelectAllCbx2" name="select-all" value="true" type="checkbox">
							<label for="J_SelectAllCbx2"></label>
						</div>
						<span>全选</span>
					</div>
					<script type="text/javascript">
					/* 全选 */
						function bigcheaked(obj){
							if (obj.checked == false) {
					            var smObj = document.getElementsByName("items[]");
					            var x = 0;
					            for (var i = 0; i < smObj.length; i++){
					                smObj[i].checked = false;
					            }
					            $("#J_Total").text(x);
					            return false;
					        }
							if (obj.checked == true) {
					            var smObj1 = document.getElementsByName("items[]");
					            var x = 0;
					            console.log(smObj1);
					            var arr = [];
					            for (var j = 0; j < smObj1.length; j++)
					                smObj1[j].checked = true;
					            	
					        	
								var arr1 = convertToArray(smObj1);
								for(var j = 0; j < arr1.length; j++){
									var b = arr1[j].value;
									x+=parseInt(b);
									
								}
								$("#J_Total").text(x);
								console.log(x);
							return false;
							}
							}
						
						/* 单个选中 */
						function smallcheaked(obj){
							if (obj.checked == false) {
								var x = $("#J_Total").text();
								var x1 = parseInt(x);
						         var b = obj.value;
						         x1-=parseInt(b);
						         $("#J_Total").text(x1);
					            return false;
					        }
							if (obj.checked == true) {
								 console.log(obj);
								var x = $("#J_Total").text();
								var x1 = parseInt(x);
						         var b = obj.value;
						         x1+=parseInt(b);
						         $("#J_Total").text(x1);
						         return false;
							}
						}
						
						
							//nodelist转数组通过枚举
							function convertToArray(nodes){
							 var array = null;
							 try {
							 array = Array.prototype.slice.call(nodes, 0); //针对非 IE 浏览器
							 } catch (ex) {
							 array = new Array();
							 for (var i=0, len=nodes.length; i < len; i++){
							 array.push(nodes[i]);
							 }
							 }
							 return array;
							} 
							
							//结算
							function jiesuan(){
								var ddid = [];
								var obj = document.getElementsByName("items[]");
								for (var i = 0; i < obj.length; i++) {
									if(obj[i].checked == true){
										ddid.push(obj[i].alt);
									}
								}
								var uid = "${u.id}";
								console.log(ddid);
								$("#frm").attr("action","${ctx}/ts/pay.do?id=${u.id}&ddid="+ddid.join(",")+"")
								$("#frm").submit();
							}
					</script>
					<form id="frm" method="post"></form>
					<div class="operations">
						<a href="#" hidefocus="true" class="deleteAll">删除</a>
					</div>
					<div class="float-bar-right">
						<div class="amount-sum">
							<span class="txt">已选商品</span>
							<div class="arrow-box">
								<span class="selected-items-arrow"></span>
								<span class="arrow"></span>
							</div>
						</div>
						<div class="price-sum">
							<span class="txt">合计:</span>
							<strong class="price">¥<em id="J_Total">0.00</em></strong>
						</div>
						<div class="btn-area">
							<a href="#" onclick="jiesuan()" id="J_Go" class="submit-btn submit-btn-disabled" aria-label="请注意如果没有选择宝贝，将无法结算">
								<span>结&nbsp;算</span></a>
						</div>
					</div>

				</div>

				<div class="footer">
					<div class="footer-bd">
						<p>
							<em>© 2015-2025 Hengwang.com 版权所有</em>
						</p>
					</div>
				</div>

			</div>


</body>
</html>