<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("ctx", request.getContextPath());
%>

<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/common/includePub.jsp"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="Shortcut Icon" href="${ctx}/resource/image/favicon.ico" />
<title>注册页面</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="stylesheet" href="${ctx}/resource/css/dlstyle.css" />
</head>

<body>
	<div class="login-boxtitle">
		<a href="${ctx }/index.jsp"><img alt=""
			src="${ctx}/resource/images/logobig.jpg" /></a>
	</div>

	<div class="res-banner">
		<div class="res-main">
			<div class="login-banner-bg">
				<span></span><img src="${ctx}/resource/images/big.jpg" />
			</div>
			<div class="login-box">

				<div class="am-tabs" id="doc-my-tabs">
					<ul class="am-tabs-nav am-nav am-nav-tabs am-nav-justify">
						<li class="am-active"><a href="">邮箱注册</a></li>
						<li><a href="">手机号注册</a></li>
					</ul>

					<div class="am-tabs-bd">
						<div class="am-tab-panel am-active">
							<form method="post">
								<div class="user-name">
									<label for="name"><i class="am-icon-user"></i></label> <input
										type="text" name="" id="username1" placeholder="请输入姓名">
								</div>
								<div class="user-email">
									<label for="email"><i class="am-icon-envelope-o"></i></label> <input
										type="email" name="" id="email" placeholder="请输入邮箱账号">
								</div>
								<div class="user-pass">
									<label for="password"><i class="am-icon-lock"></i></label> <input
										type="password" name="" id="password1" placeholder="设置密码">
								</div>
								<div class="user-pass">
									<label for="passwordRepeat"><i class="am-icon-lock"></i></label>
									<input type="password" name="" id="passwordRepeat1"
										placeholder="确认密码">
								</div>

							</form>

							<div class="login-links">
								<label for="reader-me"> <input id="reader-me1"
									type="checkbox" value="ok"> 点击表示您同意商城《服务协议》
								</label>
							</div>
							<div class="am-cf" >
								<input type="submit" onclick="yxzc()" name="" value="注册"
									class="am-btn am-btn-primary am-btn-sm am-fl">
							</div>

						</div>

						<div class="am-tab-panel">
							<form id="frm" method="post">
								<div class="user-name">
									<label for="name"><i class="am-icon-user"></i></label> <input
										type="text" name="" id="username2" placeholder="请输入姓名">
								</div>
								<div class="user-phone">
									<label for="phone"><i
										class="am-icon-mobile-phone am-icon-md"></i></label> <input type="tel"
										name="" id="phone" placeholder="请输入手机号">
								</div>
								<div class="verification">
									<label for="code"><i class="am-icon-code-fork"></i></label> <input
										type="tel" name="" id="code" placeholder="请输入验证码"> <a
										class="btn" href="javascript:void(0);"
										onclick="sendMobileCode();" id="sendMobileCode"> <span
										id="dyMobileButton">获取</span></a>
								</div>
								<div class="user-pass">
									<label for="password"><i class="am-icon-lock"></i></label> <input
										type="password" name="" id="password2" placeholder="设置密码">
								</div>
								<div class="user-pass">
									<label for="passwordRepeat"><i class="am-icon-lock"></i></label>
									<input type="password" name="" id="passwordRepeat2"
										placeholder="确认密码">
								</div>
							</form>
							<div class="login-links">
								<label for="reader-me"> <input id="reader-me2"
									type="checkbox" value="ok"> 点击表示您同意商城《服务协议》
								</label>
							</div>
							<div class="am-cf">
								<input type="submit" onclick="sjhzc()" name="" value="注册"
									class="am-btn am-btn-primary am-btn-sm am-fl">
							</div>

							<hr>
						</div>

						<script>
							$(function() {
								$('#doc-my-tabs').tabs();
							})
							function yxzc(){
								//console.log($("#reader-me1").val())
								if($("#reader-me1").val()!='ok'){
									return false;
								}
								$.ajax({
									url:"${ctx}/zc.do",
									data:{
										name:$("#username1").val(),
										password:$("#password1").val(),
										yx:$("#email").val()
									},
									success:function(data){
										if(data!=null&&data=="success"){
											alert("注册成功请登录")
											$("#frm").attr("action","${ctx}/login.jsp")
											$("#frm").submit();
										}
									}
								})
							}
							function sjhzc(){
								//console.log($("#reader-me2").val())
								if($("#reader-me2").val()!='ok'){
									return false;
								}
								alert(1231230);
								$.ajax({
									url:"${ctx}/zc.do",
									data:{
										name:$("#username2").val(),
										password:$("#password2").val(),
										phone:$("#phone").val(),
									},
									success:function(data){
										if(data!=null&&data=="success"){
											alert("注册成功请登录")
											$("#frm").attr("action","${ctx}/login.jsp")
											$("#frm").submit();
										}
									}
								})
							}
						</script>

					</div>
				</div>

			</div>
		</div>

		
</body>
</html>