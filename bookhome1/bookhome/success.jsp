<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("ctx", request.getContextPath());
%>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>付款成功页面</title>
<link rel="stylesheet" type="text/css"
	href="${ctx}/resource/AmazeUI-2.4.2/assets/css/amazeui.css" />
<link href="${ctx}/resource/AmazeUI-2.4.2/assets/css/admin.css"
	rel="stylesheet" type="text/css">
<link href="${ctx}/resource/basic/css/demo.css" rel="stylesheet"
	type="text/css" />

<link href="${ctx}/resource/css/sustyle.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/resource/basic/js/jquery-1.7.min.js"></script>

</head>

<body>


	<%@ include file="common/daohan.jsp"%>
	<div class="clear"></div>


	<div class="take-delivery">
		<div class="status">
			<h2>您已成功付款</h2>
			<div class="successInfo">
				<ul>
					<li>付款金额<em>¥${uad.jg}</em></li>
					<div class="user-info">
						<p>收货人：${uad.shr}</p>
						<p>联系电话：${uad.sjh}</p>
						<p>收货地址：${uad.xxdz}</p>
					</div>
					请认真核对您的收货信息，如有错误请联系客服
				</ul>
				<div class="option">
					<span class="info">您可以</span> <a
						href="${ctx}/order.do?id=${u.id}" class="J_MakePoint">查看<span>已买到的宝贝</span></a>
					<a href="${ctx}/person/orderinfo.jsp" class="J_MakePoint">查看<span>交易详情</span></a>
				</div>
			</div>
		</div>
	</div>


	<div class="footer">
		<div class="footer-hd">
			<p>
				<a href="#">恒望科技</a> <b>|</b> <a href="#">商城首页</a> <b>|</b> <a
					href="#">支付宝</a> <b>|</b> <a href="#">物流</a>
			</p>
		</div>
		<div class="footer-bd">
			<p>
				<a href="#">关于恒望</a> <a href="#">合作伙伴</a> <a href="#">联系我们</a> <a
					href="#">网站地图</a> <em>© 2015-2025 Hengwang.com 版权所有</em>
			</p>
		</div>
	</div>


</body>
</html>
